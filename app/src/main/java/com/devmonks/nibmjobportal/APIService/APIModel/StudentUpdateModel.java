package com.devmonks.nibmjobportal.APIService.APIModel;

public class StudentUpdateModel {
    private String name;
    private String email;
    private String about;
    private String highest_qualification;
    private String qualified_year;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getHighest_qualification() {
        return highest_qualification;
    }

    public void setHighest_qualification(String highest_qualification) {
        this.highest_qualification = highest_qualification;
    }

    public String getQualified_year() {
        return qualified_year;
    }

    public void setQualified_year(String qualified_year) {
        this.qualified_year = qualified_year;
    }
}
