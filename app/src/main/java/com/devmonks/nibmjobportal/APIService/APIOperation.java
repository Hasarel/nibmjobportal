package com.devmonks.nibmjobportal.APIService;

import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.dasbikash.android_network_monitor.NetworkMonitor;
import com.devmonks.nibmjobportal.APIService.APIModel.PasswordUpdateModel;
import com.devmonks.nibmjobportal.APIService.APIModel.RegisterModel;
import com.devmonks.nibmjobportal.APIService.APIModel.ResetPasswordModel;
import com.devmonks.nibmjobportal.APIService.APIModel.SocialUrlModel;
import com.devmonks.nibmjobportal.APIService.APIModel.StudentUpdateModel;
import com.devmonks.nibmjobportal.APIService.APIModel.StudentVerificationModel;
import com.devmonks.nibmjobportal.APIService.Interfaces.EndPoints;
import com.devmonks.nibmjobportal.BuildConfig;
import com.devmonks.nibmjobportal.Firebase.FirebaseOP;
import com.devmonks.nibmjobportal.Model.CVDocumentModel;
import com.devmonks.nibmjobportal.Model.JobCategoryModel;
import com.devmonks.nibmjobportal.Model.JobVacancyModel;
import com.devmonks.nibmjobportal.Model.SkillModel;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.Util.Constraints;
import com.devmonks.nibmjobportal.Util.RetrofitClient;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class APIOperation {
    //Singleton Instance
    private static final APIOperation instance = new APIOperation();
    private final String TAG = "API_Operation";
    EndPoints endPoints;
    public final String VLF_BASE_URL;
    public final String VERIFICATION_URL = "https://circuito-api.azurewebsites.net/JobPortal/FindStudent";
    public final String VERIFICATION_APIKEY = "CeyDWf9s4C";

    //Singleton Constructor
    private APIOperation() {
        endPoints = RetrofitClient.getLoginClient().create(EndPoints.class);
        VLF_BASE_URL = BuildConfig.API_URL;
    }

    //Singleton request instance
    public static APIOperation getInstance() {
        return instance;
    }

    /**
     * Checks for a available internet connection
     *
     * @return returns the connected status
     */
    private boolean checkConnection() {
        return NetworkMonitor.isConnected();
    }

    /**
     * Student based Operations
     */

    /**
     * Integrated with newer API endpoint received from NIBM
     *
     * @param nic      NIC number of Student
     * @param callback Callback interface
     * @throws JSONException JSON parse exception
     */
    public void getStudentVerification(String nic, OnAPIResultCallback callback) throws JSONException {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("authKey", VERIFICATION_APIKEY);
        jsonObject.put("nicNo", nic);
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<StudentVerificationModel> call = endPoints.getStudentVerification(VERIFICATION_URL, body);
        call.enqueue(new Callback<StudentVerificationModel>() {
            @Override
            public void onResponse(Call<StudentVerificationModel> call, Response<StudentVerificationModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onStudentVerification(response.body());
                    } else {
                        Log.e(TAG, "Null Response Body!");
                        callback.onOperationFailed("Student Verification failed. Please try again!");
                    }
                } else if (response.code() == 401) {
                    callback.onOperationFailed("Student already registered!");
                } else if (response.code() == 404) {
                    callback.onOperationFailed("Student not found. Please contact NIBM!");
                } else {
                    callback.onOperationFailed("Error occurred while loading Student Verification");
                }
            }

            @Override
            public void onFailure(Call<StudentVerificationModel> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Error occurred while loading Student tVerification");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param txtNIC      NIC number of student
     * @param txtName     Name of the student
     * @param txtEmail    Email address of student
     * @param txtPassword Password of the created account
     * @param callback    Callback interface
     * @throws JSONException JSON parse exception
     */
    public void registerNewStudent(String txtNIC, String txtName, String txtEmail, String txtPassword, OnAPIResultCallback callback) throws JSONException {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("nic", txtNIC.toLowerCase());
        jsonObject.put("name", txtName);
        jsonObject.put("email", txtEmail);
        jsonObject.put("password", txtPassword);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<RegisterModel> call = endPoints.studentRegister(VLF_BASE_URL + "/api/Student/register", body);
        call.enqueue(new Callback<RegisterModel>() {
            @Override
            public void onResponse(Call<RegisterModel> call, Response<RegisterModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onStudentRegister(response.body());
                    } else {
                        callback.onOperationFailed("Student Registration failed. Please try again!");
                    }
                } else if (response.code() == 401) {
                    callback.onOperationFailed("Student already registered!");
                } else {
                    callback.onOperationFailed("Error occurred while Student Registration!");
                }
            }

            @Override
            public void onFailure(Call<RegisterModel> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("User Registration failed. Please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic      Student NIC number
     * @param password Account password
     * @param callback Callback interface
     * @throws JSONException JSON parse exception
     */
    public void postUserLogin(String nic, String password, OnAPIResultCallback callback) throws JSONException {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("nic", nic.toLowerCase());
        jsonObject.put("password", password);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<UserModel> call = endPoints.studentLogin(VLF_BASE_URL + "/api/Student/login", body);
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onLoginResponse(response.body());
                    } else {
//                        callback.onOperationFailed("Null Response Body!");
                        Log.e(TAG, "Null Response Body!");
                        callback.onOperationFailed("User login failed. Please try again!");
                    }
                } else if (response.code() == 401) {
                    Log.e(TAG, "Unauthorized: Invalid Login credentials");
                    callback.onOperationFailed("Invalid NIC or Password!");
                } else if (response.code() == 404) {
                    Log.e(TAG, "User not found");
                    callback.onOperationFailed("User not Registered! Please Signup!");
                } else {
                    Log.e(TAG, "Invalid response code " + response.code());
                    callback.onOperationFailed("User login failed. Please try again!");
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                Log.e(TAG, t.getMessage());
//                callback.onOperationFailed("Response on failure " + t.getMessage());
                callback.onOperationFailed("User login failed. Please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic       NIC NUmber of the student
     * @param studentID Student ID
     * @param callback  Callback interface
     */
    public void getCVList(String nic, String studentID, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }
        Call<ArrayList<CVDocumentModel>> call = endPoints.getCVList(VLF_BASE_URL + "/api/Cv/getallcv", studentID);
        call.enqueue(new Callback<ArrayList<CVDocumentModel>>() {
            @Override
            public void onResponse(Call<ArrayList<CVDocumentModel>> call, Response<ArrayList<CVDocumentModel>> response) {
                if (response.code() == 200) {
                    if (response.body() != null && !response.body().isEmpty()) {
                        callback.onCVListLoaded(response.body());
                    } else {
                        callback.onCVListLoaded(new ArrayList<>());
                    }
                }
                //Handling No CV for initial CV load
                else if (response.code() == 404) {
                    callback.onCVListLoaded(new ArrayList<>());
                } else {
                    Log.e(TAG, "Invalid response code " + response.code());
                    callback.onOperationFailed("Error occurred while loading CV list!");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<CVDocumentModel>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Could not CV list please try again!");
            }
        });
    }

    /**
     * Remove the CV document object from the firebase storage bucket
     *
     * @param nic      NIC number of the student
     * @param _id      CV File ID
     * @param file_url File URL
     * @param callback Callback interface
     */
    public void removeCV(String nic, String _id, String file_url, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        //Opting out the delete operation of the CV from Firebase Bucket
        deleteCVFromServer(nic.toUpperCase(), _id, callback);

//        FirebaseOP.getInstance().removeCV(file_url, new FirebaseOP.FirebaseOPListener() {
//            @Override
//            public void onCVRemoved() {
//                deleteCVFromServer(nic.toUpperCase(), _id, callback);
//            }
//
//            @Override
//            public void onFirebaseOperationFailed(String message) {
//                Log.e(TAG, message);
//                callback.onOperationFailed("Could not remove CV, please try again!");
//            }
//        });

    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic      NIC number of the student
     * @param cvID     CV File ID
     * @param callback Callback interface
     */
    private void deleteCVFromServer(String nic, String cvID, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }
        Call<JSONObject> call = endPoints.deleteCV(VLF_BASE_URL + "/api/Cv/deletecv", cvID);
        call.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if (response.code() == 200) {
                    callback.onCVRemoved();
                } else {
                    Log.e(TAG, response.toString());
                    callback.onOperationFailed("Could not remove CV, please try again!");
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Could not remove CV, please try again!");
            }
        });
    }

    /**
     * Upload the CV Document to the firebase storage bucket
     *
     * @param nic        NIC Number of the student
     * @param file_name  File name of the CV
     * @param alias      File alias for easy recognition
     * @param added_date Added date of the document
     * @param file       File URI
     * @param callback   Callback interface
     */
    public void uploadCVDocument(String nic, String studentID, String file_name, String alias, String added_date, Uri file, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        FirebaseOP.getInstance().uploadCV(String.format(Locale.ENGLISH, "%s_%s", nic.toUpperCase(), Calendar.getInstance().getTimeInMillis()), file, new FirebaseOP.FirebaseOPListener() {
            @Override
            public void onCVUploaded(String url) {
                try {
                    if (url == null) {
                        Log.e(TAG, "CV file URL is not found");
                        callback.onOperationFailed("Failed to upload CV. Please try again!");
                        return;
                    }

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("document_url", url);
                    jsonObject.put("file_name", file_name);
                    jsonObject.put("alias", alias);
                    jsonObject.put("added_date", added_date);
                    jsonObject.put("id", studentID);
                    addCVDocument(jsonObject, callback);

                } catch (JSONException e) {
                    e.printStackTrace();
                    FirebaseCrashlytics.getInstance().recordException(e);
                    callback.onOperationFailed("Failed to upload CV. Please try again!");
                }
            }

            @Override
            public void onFirebaseOperationFailed(String message) {
                Log.e(TAG, message);
                callback.onOperationFailed("Failed to upload CV. Please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param jsonObject JSON object which contains the request body
     * @param callback   Callback interface
     */
    private void addCVDocument(JSONObject jsonObject, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<JSONObject> call = endPoints.uploadCV(VLF_BASE_URL + "/api/Cv/save", body);
        call.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if (response.code() == 200) {
                    callback.onCVAdded();
                } else {
                    Log.e(TAG, response.toString());
                    callback.onOperationFailed("Could not upload CV, please try again!");
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Could not upload CV, please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic       NIC number of the student
     * @param studentID Student ID
     * @param password  Access password
     * @param callback  Callback interface
     * @throws JSONException JSON parse exception
     */
    public void updatePassword(String nic, String studentID, String password, OnAPIResultCallback callback) throws JSONException {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("password", password);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<PasswordUpdateModel> call = endPoints.passwordUpdate(VLF_BASE_URL + "/api/Student/profile", body, studentID);
        call.enqueue(new Callback<PasswordUpdateModel>() {
            @Override
            public void onResponse(Call<PasswordUpdateModel> call, Response<PasswordUpdateModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onPasswordUpdated(response.body());
                    } else {
                        callback.onOperationFailed("Failed to change password. Please try again!");
                    }
                } else {
                    Log.e(TAG, response.toString());
                    callback.onOperationFailed("Error occurred while changing Password!");
                }
            }

            @Override
            public void onFailure(Call<PasswordUpdateModel> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Could not change Password. Please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic      NIC Number of the student
     * @param callback Callback interface
     */
    public void requestOTP(String nic, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        Call<ResetPasswordModel> call = endPoints.resetPasswordRequest(VLF_BASE_URL + "/api/student/password/", nic);
        call.enqueue(new Callback<ResetPasswordModel>() {
            @Override
            public void onResponse(Call<ResetPasswordModel> call, @NonNull Response<ResetPasswordModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onResetOtpReceived(response.body().getOtp(), response.body().getEmail(), response.body().getId());
                    } else {
                        Log.e(TAG, "Null Response Body!");
                        callback.onOperationFailed("Failed to reset password. Please try again!");
                    }
                } else if (response.code() == 404) {
                    callback.onOperationFailed("Student not found!");
                } else {
                    callback.onOperationFailed("Error occurred while resetting password!");
                }
            }

            @Override
            public void onFailure(Call<ResetPasswordModel> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Error occurred while resetting password!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic           student NIC number
     * @param studentID     Student ID
     * @param about         Student About caption
     * @param name          Student name
     * @param email         Student email address
     * @param qualification Student Qualification
     * @param qualifiedYear Student qualified year
     * @param callback      Callback interface
     * @throws JSONException JSON parse exception
     */
    public void updateStudentInfo(String nic, String studentID, String about, String name, String email, String qualification, String qualifiedYear, OnAPIResultCallback callback) throws JSONException {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("id", studentID);
        jsonObject.put("name", name);
        jsonObject.put("email", email);
        if (qualification != null)
            jsonObject.put("highest_qualification", qualification);
        if (qualifiedYear != null)
            jsonObject.put("qualified_year", qualifiedYear);
        if (about != null)
            jsonObject.put("about", about);

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<JSONObject> call = endPoints.studentUpdate(VLF_BASE_URL + "/api/Student/updateprofile", body);
        call.enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onStudentInformationUpdated(null);
                    } else {
                        callback.onOperationFailed("Failed to update information. Please try again!");
                    }
                } else {
                    Log.e(TAG, response.toString());
                    callback.onOperationFailed("Error occurred while Updating information!");
                }
            }

            @Override
            public void onFailure(Call<JSONObject> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Could not update information. Please try again!");
            }
        });
    }

    /**
     * Upload the image to the firebase storage bucket
     *
     * @param nic       Student NIC number
     * @param imageData imagedata of the student profile image as a byte []
     * @param callback  Callback interface
     */
    public void updateProfileImage(String nic, String studentID, byte[] imageData, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        FirebaseOP.getInstance().uploadProfileImage(nic, imageData, new FirebaseOP.FirebaseOPListener() {
            @Override
            public void onImageUploaded(String url) {
                if (url != null) {
                    updateProfileImageURL(nic, studentID, url, callback);
                } else {
                    callback.onOperationFailed("Failed to update Profile image. Please try again!");
                }
            }

            @Override
            public void onFirebaseOperationFailed(String message) {
                Log.e(TAG, message);
                callback.onOperationFailed("Failed to update Profile image. Please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic       Student NIC number
     * @param studentID Student ID
     * @param url       Image URL
     * @param callback  Callback interface
     */
    private void updateProfileImageURL(String nic, String studentID, String url, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", studentID);
            jsonObject.put("key", "image_url");
            jsonObject.put("value", url);
            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            Call<JSONObject> call = endPoints.profileImageUpdate(VLF_BASE_URL + "/api/Student/update", body);
            call.enqueue(new Callback<JSONObject>() {
                @Override
                public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            callback.onProfileImageUpdated(url);
                        } else {
                            callback.onOperationFailed("Could not update Profile image. Please try again!");
                        }
                    } else {
                        callback.onOperationFailed("Error occurred while Updating Profile image!");
                    }
                }

                @Override
                public void onFailure(Call<JSONObject> call, Throwable t) {
                    t.printStackTrace();
                    Log.e(TAG, t.getMessage());
                    callback.onOperationFailed("Failed to update Profile image. Please try again!");
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrashlytics.getInstance().recordException(e);
            callback.onOperationFailed("Failed to update Profile image. Please try again!");
        }
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic         Student NIC
     * @param studentID   Student ID
     * @param url         Github or LinkedIn URL
     * @param isGitHubURL if gitHUB URL = true else false
     * @param callback    Callback interface
     */
    public void updateSocialURL(String nic, String studentID, String url, boolean isGitHubURL, OnAPIResultCallback callback) {
        try {
            if (!checkConnection()) {
                callback.onConnectionLost(Constraints.CONNECTION_LOST);
                return;
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", studentID);
            if (isGitHubURL) {
                jsonObject.put("key", "github_url");
            } else {
                jsonObject.put("key", "linkedin_url");
            }
            jsonObject.put("value", url);

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            Call<SocialUrlModel> call = endPoints.updateSocialURL(VLF_BASE_URL + "/api/Student/update", body);
            call.enqueue(new Callback<SocialUrlModel>() {
                @Override
                public void onResponse(Call<SocialUrlModel> call, Response<SocialUrlModel> response) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            callback.onSocialURLUpdated(url, isGitHubURL);
                        } else {
                            callback.onOperationFailed("Could not update URL. Please try again!");
                        }
                    } else {
                        callback.onOperationFailed("Error occurred while Updating URL!");
                    }
                }

                @Override
                public void onFailure(Call<SocialUrlModel> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                    callback.onOperationFailed("Failed to update URL. Please try again!");
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrashlytics.getInstance().recordException(e);
            callback.onOperationFailed("Failed to update URL. Please try again!");
        }
    }

    /**
     * Category based operations
     */

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param callback Callback interface
     */
    public void getAllCategoriesList(OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        Call<ArrayList<JobCategoryModel>> call = endPoints.getAllCategoryList(VLF_BASE_URL + "/api/Category/get");
        call.enqueue(new Callback<ArrayList<JobCategoryModel>>() {
            @Override
            public void onResponse(Call<ArrayList<JobCategoryModel>> call, Response<ArrayList<JobCategoryModel>> response) {
                if (response.code() == 200) {
                    if (response.body() != null && !response.body().isEmpty()) {
                        callback.onJobCategoriesLoaded(response.body());
                    } else {
                        callback.onJobCategoriesLoaded(new ArrayList<>());
                    }
                }
                //Handling No categories for initial profile load
                else if (response.code() == 404) {
                    callback.onJobCategoriesLoaded(new ArrayList<>());
                } else {
//                    callback.onOperationFailed("Invalid response code " + response.code());
                    Log.e(TAG, "Invalid response code " + response.code());
                    callback.onOperationFailed("Error occurred while loading Categories!");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<JobCategoryModel>> call, Throwable t) {
//                callback.onOperationFailed("Response on failure " + t.getMessage());
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Could not load Categories please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic       Student NIC
     * @param studentID Student ID
     * @param callback  Callback interface
     */
    public void getSelectedJobCategoryList(String nic, String studentID, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        Call<ArrayList<JobCategoryModel>> call = endPoints.getSelectedJobCategoryList(VLF_BASE_URL + "/api/Student/categories", studentID);
        call.enqueue(new Callback<ArrayList<JobCategoryModel>>() {
            @Override
            public void onResponse(Call<ArrayList<JobCategoryModel>> call, Response<ArrayList<JobCategoryModel>> response) {
                if (response.code() == 200) {
                    if (response.body() != null && !response.body().isEmpty()) {
                        callback.onJobCategoriesLoaded(response.body());
                    } else {
                        callback.onJobCategoriesLoaded(new ArrayList<>());
                    }
                }
                //Handling No categories for initial profile load
                else if (response.code() == 404) {
                    callback.onJobCategoriesLoaded(new ArrayList<>());
                } else {
//                    callback.onOperationFailed("Invalid response code " + response.code());
                    Log.e(TAG, "Invalid response code " + response.code());
                    callback.onOperationFailed("Error occurred while loading Categories!");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<JobCategoryModel>> call, Throwable t) {
//                callback.onOperationFailed("Response on failure " + t.getMessage());
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Could not load Categories please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param jobCategoriesList nre job categories list
     * @param nic               Student NIC
     * @param studentID         Student ID
     * @param callback          Callback interface
     */
    public void updateSelectedCategories(ArrayList<JobCategoryModel> jobCategoriesList, String nic, String studentID, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        StringBuilder categories = new StringBuilder();
        for (int i = 0; i < jobCategoriesList.size(); i++) {
            if (i == 0) {
                categories.append(jobCategoriesList.get(i).getName());
                continue;
            }
            categories.append(",").append(jobCategoriesList.get(i).getName());
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", studentID);
            jsonObject.put("key", "categories");
            if (categories.length() > 0)
                jsonObject.put("value", categories.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            callback.onOperationFailed("Could not update Categories. Please try again!");
        }

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<JobCategoryModel> call = endPoints.updateSelectedJobCategories(VLF_BASE_URL + "/api/Student/update", body);
        call.enqueue(new Callback<JobCategoryModel>() {
            @Override
            public void onResponse(Call<JobCategoryModel> call, Response<JobCategoryModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onSelectedCategoriesUpdated(jobCategoriesList);
                    } else {
                        callback.onOperationFailed("Failed to update Categories. Please try again!");
                    }
                } else {
                    Log.e(TAG, response.toString());
                    callback.onOperationFailed("Error occurred while Updating Categories!");
                }
            }

            @Override
            public void onFailure(Call<JobCategoryModel> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Could not update Categories. Please try again!");
            }
        });
    }

    /**
     * Skill based operations
     */

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param callback Callback interface
     */
    public void getAllSkillsList(OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        Call<ArrayList<SkillModel>> call = endPoints.getAllSkillsList(VLF_BASE_URL + "/api/Skill/get");
        call.enqueue(new Callback<ArrayList<SkillModel>>() {
            @Override
            public void onResponse(Call<ArrayList<SkillModel>> call, Response<ArrayList<SkillModel>> response) {
                if (response.code() == 200) {
                    if (response.body() != null && !response.body().isEmpty()) {
                        callback.onAllSkillsLoaded(response.body());
                    } else {
                        callback.onOperationFailed("No Skills found!");
                    }
                }
                //Handling No categories for initial profile load
                else if (response.code() == 404) {
                    callback.onAllSkillsLoaded(new ArrayList<>());
                } else {
//                    callback.onOperationFailed("Invalid response code " + response.code());
                    Log.e(TAG, "Invalid response code " + response.code());
                    callback.onOperationFailed("Error occurred while loading Skills!");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SkillModel>> call, Throwable t) {
//                callback.onOperationFailed("Response on failure " + t.getMessage());
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Could not load Skills please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param skillList new skill list
     * @param nic       Student NIC
     * @param studentID Student ID
     * @param callback  Callback interface
     */
    public void updateSelectedSkills(ArrayList<SkillModel> skillList, String nic, String studentID, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        StringBuilder skills = new StringBuilder();
        for (int i = 0; i < skillList.size(); i++) {
            if (i == 0) {
                skills.append(skillList.get(i).getName());
                continue;
            }
            skills.append(",").append(skillList.get(i).getName());
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", studentID);
            jsonObject.put("key", "skills");
            jsonObject.put("value", skills.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            Log.e(TAG, "JSON Parse Error");
            callback.onOperationFailed("Could not update Skills. Please try again!");
        }
        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<SkillModel> call = endPoints.updateSelectedSkills(VLF_BASE_URL + "/api/Student/update", body);
        call.enqueue(new Callback<SkillModel>() {
            @Override
            public void onResponse(Call<SkillModel> call, Response<SkillModel> response) {
                if (response.code() == 200) {
                    if (response.body() != null) {
                        callback.onSelectedSkillsUpdated(skillList);
                    } else {
                        callback.onOperationFailed("Failed to update Skills. Please try again!");
                    }
                } else {
                    Log.e("Error Response : ", response.toString());
                    callback.onOperationFailed("Error occurred while Updating Skills!");
                }
            }

            @Override
            public void onFailure(Call<SkillModel> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                Log.e(TAG, "Server Error");
                callback.onOperationFailed("Could not update Skills. Please try again!");
            }
        });
    }

    /**
     * Job post based operations
     */

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param categoryID Category ID
     * @param callback   Callback interface
     */
    public void getJobsBasedOnCategory(String categoryID, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        Call<ArrayList<JobVacancyModel>> call = endPoints.getJobPostListByCategory(VLF_BASE_URL + "/api/Job/category", categoryID);
        call.enqueue(new Callback<ArrayList<JobVacancyModel>>() {
            @Override
            public void onResponse(Call<ArrayList<JobVacancyModel>> call, Response<ArrayList<JobVacancyModel>> response) {
                if (response.code() == 200) {
                    if (response.body() != null && !response.body().isEmpty()) {
                        callback.onFilteredJobsLoaded(response.body());
                    } else {
                        callback.onFilteredJobsLoaded(new ArrayList<>());
                    }
                } else {
//                    callback.onOperationFailed("Invalid response code " + response.code());
                    Log.e(TAG, "Invalid response code " + response.code());
                    callback.onOperationFailed("Error occurred while loading Jobs!");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<JobVacancyModel>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
//                callback.onOperationFailed("Response on failure " + t.getMessage());
                callback.onOperationFailed("Could not load Jobs please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic       Student NIC Number
     * @param studentID Student ID
     * @param callback  Callback interface
     */
    public void getJobPostsFromAPI(String nic, String studentID, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }

        Call<ArrayList<JobVacancyModel>> call = endPoints.getAllJobPostList(VLF_BASE_URL + "/api/Job/getbycategory", studentID);
        call.enqueue(new Callback<ArrayList<JobVacancyModel>>() {
            @Override
            public void onResponse(Call<ArrayList<JobVacancyModel>> call, Response<ArrayList<JobVacancyModel>> response) {
                if (response.code() == 200) {
                    if (response.body() != null && !response.body().isEmpty()) {
                        callback.onJobPostsLoaded(response.body());
                    } else {
                        callback.onJobPostsLoaded(new ArrayList<>());
                    }
                }
                //Handling No jobs for initial profile load
                else if (response.code() == 404) {
                    callback.onJobPostsLoaded(new ArrayList<>());
                } else {
//                    callback.onOperationFailed("Invalid response code " + response.code());
                    Log.e(TAG, "Invalid response code " + response.toString());
                    callback.onOperationFailed("Error occurred while loading Jobs!");
                }
            }

            @Override
            public void onFailure(Call<ArrayList<JobVacancyModel>> call, Throwable t) {
                Log.e(TAG, t.getMessage());
//                callback.onOperationFailed("Response on failure " + t.getMessage());
                callback.onOperationFailed("Could not load Jobs please try again!");
            }
        });
    }

    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic       Student NIC Number
     * @param studentID Student ID
     * @param cvUrl     URL of the CV Document
     * @param jobPost   Job Post ID
     * @param callback  Callback interface
     */
    public void applyForJob(String nic, String studentID, String cvUrl, String jobPost, OnAPIResultCallback callback) {
        try {
            if (!checkConnection()) {
                callback.onConnectionLost(Constraints.CONNECTION_LOST);
                return;
            }
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("id", studentID);
            jsonObject.put("cv_url", cvUrl);
            jsonObject.put("job_post", jobPost);
            jsonObject.put("date", new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).format(new Date()));

            RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
            Call<JSONObject> call = endPoints.applyJob(VLF_BASE_URL + "/api/Job/apply", body);
            call.enqueue(new Callback<JSONObject>() {
                @Override
                public void onResponse(Call<JSONObject> call, Response<JSONObject> response) {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            callback.onApplicationSuccess();
                        } else {
                            callback.onOperationFailed("Could not apply for job. Please try again!");
                        }
                    } else if (response.code() == 401) {
                        callback.onOperationFailed("You have already applied to this position!");
                    } else {
                        callback.onOperationFailed("Error occurred while submitting application!");
                    }
                }

                @Override
                public void onFailure(Call<JSONObject> call, Throwable t) {
                    Log.e(TAG, t.getMessage());
                    callback.onOperationFailed("Failed to apply for job. Please try again!");
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrashlytics.getInstance().recordException(e);
            callback.onOperationFailed("Failed to apply for job. Please try again!");
        }
    }


    /**
     * Integrated with newer API endpoint deployed on Azure
     *
     * @param nic       Student NIC Number
     * @param studentID Student ID
     * @param jobPost   Job Post ID
     * @param callback  Callback interface
     */
    public void checkJobStatus(String nic, String studentID, String jobPost, OnAPIResultCallback callback) {
        if (!checkConnection()) {
            callback.onConnectionLost(Constraints.CONNECTION_LOST);
            return;
        }
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("id", studentID);
            jsonObject.put("job_post", jobPost);
        } catch (JSONException e) {
            e.printStackTrace();
            FirebaseCrashlytics.getInstance().recordException(e);
            callback.onOperationFailed("Failed to load job information. Please try again!");
        }

        RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), jsonObject.toString());
        Call<Boolean> call = endPoints.checkJobStatus(VLF_BASE_URL + "/api/Job/checkstatus", body);
        call.enqueue(new Callback<Boolean>() {
            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.code() == 200) {
                    callback.onJobStatusLoaded(response.body());
                } else if (response.code() == 401) {
                    callback.onJobStatusLoaded(true);
                    Log.e(TAG, response.toString());
                } else {
                    callback.onOperationFailed("Error occurred while loading Job information!");
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                Log.e(TAG, t.getMessage());
                callback.onOperationFailed("Failed to load job information. Please try again!");
            }
        });
    }

    /**
     * Callback Interface & methods
     */
    public interface OnAPIResultCallback {
        default void onJobPostsLoaded(ArrayList<JobVacancyModel> jobPosts) {
        }

        default void onJobCategoriesLoaded(ArrayList<JobCategoryModel> jobCategories) {
        }

        default void onJobStatusLoaded(boolean applied) {
        }

        default void onApplicationSuccess() {
        }

        default void onCVListLoaded(ArrayList<CVDocumentModel> cvList) {
        }

        default void onCVRemoved() {
        }

        default void onCVAdded() {
        }

        default void onAllSkillsLoaded(ArrayList<SkillModel> skillList) {
        }

        default void onFilteredJobsLoaded(ArrayList<JobVacancyModel> jobPosts) {
        }

        default void onLoginResponse(UserModel UserModel) {
        }

        default void onStudentVerification(StudentVerificationModel model) {
        }

        default void onStudentRegister(RegisterModel model) {
        }

        default void onSocialURLUpdated(String url, boolean isGitHubURL) {
        }

        default void onProfileImageUpdated(String url) {
        }

        default void onSelectedCategoriesUpdated(ArrayList<JobCategoryModel> updatedJobCategoriesList) {
        }

        default void onSelectedSkillsUpdated(ArrayList<SkillModel> skillList) {
        }

        default void onStudentInformationUpdated(StudentUpdateModel studentData) {
        }

        default void onPasswordUpdated(PasswordUpdateModel result) {
        }

        default void onResetOtpReceived(String otp, String email, String studentID) {
        }

        //Non exceptional override methods
        void onOperationFailed(String error);

        void onConnectionLost(String message);
    }
}
