package com.devmonks.nibmjobportal.APIService.Interfaces;

import com.devmonks.nibmjobportal.Model.CVDocumentModel;
import com.devmonks.nibmjobportal.Model.JobCategoryModel;
import com.devmonks.nibmjobportal.APIService.APIModel.PasswordUpdateModel;
import com.devmonks.nibmjobportal.APIService.APIModel.RegisterModel;
import com.devmonks.nibmjobportal.APIService.APIModel.ResetPasswordModel;
import com.devmonks.nibmjobportal.Model.SkillModel;
import com.devmonks.nibmjobportal.APIService.APIModel.SocialUrlModel;
import com.devmonks.nibmjobportal.APIService.APIModel.StudentUpdateModel;
import com.devmonks.nibmjobportal.APIService.APIModel.StudentVerificationModel;
import com.devmonks.nibmjobportal.Model.JobVacancyModel;
import com.devmonks.nibmjobportal.Model.UserModel;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface EndPoints {

    //Job related API calls
    @Headers({"Content-Type: application/json"})
    @GET
    Call<ArrayList<JobVacancyModel>> getAllJobPostList(@Url String Url, @Query("id") String id);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<ArrayList<JobCategoryModel>> getSelectedJobCategoryList(@Url String Url, @Query("id") String id);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<ArrayList<JobVacancyModel>> getJobPostListByCategory(@Url String Url, @Query("id") String id);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<JSONObject> applyJob(@Url String Url , @Body RequestBody body);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<Boolean> checkJobStatus(@Url String Url, @Body RequestBody body);

    //Category related API calls
    @Headers({"Content-Type: application/json"})
    @GET
    Call<ArrayList<JobCategoryModel>> getAllCategoryList(@Url String Url);

    //Skill related API calls
    @Headers({"Content-Type: application/json"})
    @GET
    Call<ArrayList<SkillModel>> getAllSkillsList(@Url String Url);

    //User (student) related API calls
    //Todo: Check with PO
    @Headers({"Content-Type: application/json"})
    @POST
    Call<UserModel> studentLogin(@Url String Url , @Body RequestBody body);

    //Todo: Check with PO
    @Headers({"Content-Type: application/json"})
    @POST
    Call<StudentVerificationModel> getStudentVerification(@Url String Url, @Body RequestBody body);

    //Todo: Check with PO
    @Headers({"Content-Type: application/json"})
    @POST
    Call<RegisterModel> studentRegister(@Url String Url , @Body RequestBody body);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<JSONObject>studentUpdate(@Url String Url , @Body RequestBody body);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<PasswordUpdateModel>passwordUpdate(@Url String Url , @Body RequestBody body, @Query("id") String id);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<SocialUrlModel> updateSocialURL(@Url String Url, @Body RequestBody body);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<JSONObject>profileImageUpdate(@Url String Url, @Body RequestBody body);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<JobCategoryModel> updateSelectedJobCategories(@Url String Url , @Body RequestBody body);

    @Headers({"Content-Type: application/json"})
    @POST
    Call<SkillModel> updateSelectedSkills(@Url String Url , @Body RequestBody body);

    @Headers({"Content-Type: application/json"})
    @GET
    Call<ResetPasswordModel> resetPasswordRequest(@Url String Url, @Query("nic") String nic);

    //Todo: Check with PO
    @Headers({"Content-Type: application/json"})
    @GET
    Call<ArrayList<CVDocumentModel>> getCVList(@Url String Url, @Query("id") String id);

    //Todo: Check with PO
    @Headers({"Content-Type: application/json"})
    @POST
    Call<JSONObject> uploadCV(@Url String url, @Body RequestBody body);

    //Todo: Check with PO
    @Headers({"Content-Type: application/json"})
    @GET
    Call<JSONObject> deleteCV(@Url String url, @Query("id") String id);
}
