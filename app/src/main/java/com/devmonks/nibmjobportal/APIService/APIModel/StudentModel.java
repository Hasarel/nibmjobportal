package com.devmonks.nibmjobportal.APIService.APIModel;

public class StudentModel {
    private String id;
    private String nic;
    private String name;
    private String email;

    public StudentModel(String id, String nic, String name, String email) {
        this.id = id;
        this.nic = nic;
        this.name = name;
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
