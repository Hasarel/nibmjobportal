package com.devmonks.nibmjobportal.APIService.APIModel;

public class StudentVerificationModel {
    private boolean exist;
    private String nic;

    public StudentVerificationModel(boolean exist, String nic) {
        this.exist = exist;
        this.nic = nic;
    }

    public boolean isExist() {
        return exist;
    }

    public void setExist(boolean exist) {
        this.exist = exist;
    }

    public String getNic() {
        return nic;
    }

    public void setNic(String nic) {
        this.nic = nic;
    }
}
