package com.devmonks.nibmjobportal.APIService.APIModel;

import java.io.Serializable;

public class CompanyInfoModel implements Serializable {
    private String id;
    private String company_Name;
    private String logo_path;
    private String email;
    private String contact_No;
    private String description;

    public CompanyInfoModel(String id, String company_Name, String logo_path, String email, String contact_No, String description) {
        this.id = id;
        this.company_Name = company_Name;
        this.logo_path = logo_path;
        this.email = email;
        this.contact_No = contact_No;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCompany_Name() {
        return company_Name;
    }

    public void setCompany_Name(String company_Name) {
        this.company_Name = company_Name;
    }

    public String getLogo_path() {
        return logo_path;
    }

    public void setLogo_path(String logo_path) {
        this.logo_path = logo_path;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact_No() {
        return contact_No;
    }

    public void setContact_No(String contact_No) {
        this.contact_No = contact_No;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
