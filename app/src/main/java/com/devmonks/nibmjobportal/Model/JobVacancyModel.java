package com.devmonks.nibmjobportal.Model;

import com.devmonks.nibmjobportal.APIService.APIModel.CompanyInfoModel;
import com.devmonks.nibmjobportal.APIService.APIOperation;

import java.io.Serializable;

public class JobVacancyModel implements Serializable {
    protected String id;
    protected String position;
    protected String description;
    protected String jobFlyer;
    protected String closingDate;
    protected String posted_date;
    JobCategoryModel jobCategory;
    CompanyInfoModel company;

    public JobVacancyModel(String id, String position, String description, String jobFlyer, String closingDate, String posted_date, JobCategoryModel jobCategory, CompanyInfoModel company) {
        this.id = id;
        this.position = position;
        this.description = description;
        this.jobFlyer = jobFlyer;
        this.closingDate = closingDate;
        this.posted_date = posted_date;
        this.jobCategory = jobCategory;
        this.company = company;
    }

    public JobVacancyModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getJobFlyer() {
        return jobFlyer;
    }

    public void setJobFlyer(String jobFlyer) {
        this.jobFlyer = jobFlyer;
    }

    public String getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(String closingDate) {
        this.closingDate = closingDate;
    }

    public String getPosted_date() {
        return posted_date;
    }

    public void setPosted_date(String posted_date) {
        this.posted_date = posted_date;
    }

    public String getJobCategoryName() {
        return this.jobCategory.getName();
    }

    public String getCompanyName() {
        return this.company.getCompany_Name();
    }

    public String getCompanyLogo() {
        return this.company.getLogo_path();
    }

    public void applyForJob(String nic, String studentID, String document_url, String job_id, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().applyForJob(nic, studentID, document_url, job_id, callback);
    }

    public void checkJobStatus(String nic,String studentID, String job_id, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().checkJobStatus(nic, studentID, job_id, callback);
    }

    public void getJobPostsFromAPI(String nic, String studentID, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().getJobPostsFromAPI(nic, studentID, callback);
    }

    public void getJobsBasedOnCategory(String categoryID, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().getJobsBasedOnCategory(categoryID, callback);
    }
}
