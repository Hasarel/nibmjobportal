package com.devmonks.nibmjobportal.Model;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.HomeActivity;
import com.devmonks.nibmjobportal.Activity.Profile.AboutMeFragment;

import java.io.Serializable;
import java.util.ArrayList;

public class JobCategoryModel implements Serializable {
    private String id;
    private String name;
    private int count;

    public JobCategoryModel(String id, String name, int count) {
        this.id = id;
        this.name = name;
        this.count = count;
    }

    public JobCategoryModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getJob_count() {
        return count;
    }

    public void setJob_count(int count) {
        this.count = count;
    }

    public void updateSelectedCategories(ArrayList<JobCategoryModel> categoryList, String nic, String studentID, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().updateSelectedCategories(categoryList, nic, studentID, callback);
    }

    public void getSelectedJobCategoryList(String nic, String studentID, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().getSelectedJobCategoryList(nic, studentID, callback);
    }
}
