package com.devmonks.nibmjobportal.Model;

import android.net.Uri;

import com.devmonks.nibmjobportal.APIService.APIOperation;

import java.io.Serializable;

public class CVDocumentModel implements Serializable {
    private String id;
    private String document_url;
    private String file_name;
    private String alias;
    private String added_date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDocument_url() {
        return document_url;
    }

    public void setDocument_url(String document_url) {
        this.document_url = document_url;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAdded_date() {
        return added_date;
    }

    public void setAdded_date(String added_date) {
        this.added_date = added_date;
    }


    public void uploadCVDocument(String nic, String studentID, String docName, String fileName, String date, Uri data, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().uploadCVDocument(
                nic,
                studentID,
                docName,
                fileName,
                date,
                data,
                callback
        );
    }

    public void getCVList(String nic, String studentID, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().getCVList(nic, studentID, callback);
    }

    public void removeCV(String nic, String id, String document_url, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().removeCV(nic, id, document_url, callback);
    }
}
