package com.devmonks.nibmjobportal.Model;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.Profile.UpdateProfileActivity;

import java.util.ArrayList;

public class SkillModel {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void getAllSkillsList(APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().getAllSkillsList(callback);
    }

    public void updateSelectedSkills(ArrayList<SkillModel> skillList, String nic, String studentID, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().updateSelectedSkills(skillList, nic, studentID, callback);
    }
}
