package com.devmonks.nibmjobportal.Model;

import com.devmonks.nibmjobportal.APIService.APIModel.StudentModel;
import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.Auth.RegisterActivity;
import com.devmonks.nibmjobportal.Activity.Auth.ValidateNICActivity;
import com.devmonks.nibmjobportal.Activity.Profile.AboutMeFragment;
import com.devmonks.nibmjobportal.Activity.Profile.UpdateProfileActivity;
import com.devmonks.nibmjobportal.Activity.ResetPassword.ResetPassEnterNicActivity;

import org.json.JSONException;

import java.util.List;

public class UserModel {
//    protected String id;
//    protected String nic;
//    protected String name;
//    protected String email;
    protected StudentModel student;
    protected String about;
    protected String git_url;
    protected String likedin_url;
    protected List<SkillModel> skills;
    protected List<JobCategoryModel> category;
    protected String higst_ql;
    protected String qulified_year;
    private String image_url;

    public String getId() {
        return student.getId();
    }

    public void setId(String id) {
        this.student.setId(id);
    }

    public String getNic() {
        return this.student.getNic();
    }

    public void setNic(String nic) {
        this.student.setNic(nic);
    }

    public String getName() {
        return this.student.getName();
    }

    public void setName(String name) {
        this.student.setName(name);
    }

    public String getEmail() {
        return this.student.getEmail();
    }

    public void setEmail(String email) {
        this.student.setEmail(email);
    }

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }

    public String getGit_url() {
        return git_url;
    }

    public void setGit_url(String git_url) {
        this.git_url = git_url;
    }

    public String getLikedin_url() {
        return likedin_url;
    }

    public void setLikedin_url(String likedin_url) {
        this.likedin_url = likedin_url;
    }

    public List<SkillModel> getSkills() {
        return skills;
    }

    public void setSkills(List<SkillModel> skills) {
        this.skills = skills;
    }

    public List<JobCategoryModel> getCategories() {
        return category;
    }

    public void setCategories(List<JobCategoryModel> categories) {
        this.category = categories;
    }

    public String getHigst_ql() {
        return higst_ql;
    }

    public void setHigst_ql(String higst_ql) {
        this.higst_ql = higst_ql;
    }

    public String getQulified_year() {
        return qulified_year;
    }

    public void setQulified_year(String qulified_year) {
        this.qulified_year = qulified_year;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public void userSignIn(String nic, String password, APIOperation.OnAPIResultCallback callback) throws JSONException {
        APIOperation.getInstance().postUserLogin(nic, password, callback);
    }

    public void registerNewStudent(String txtNIC, String txtName, String txtEmail, String txtPassword, APIOperation.OnAPIResultCallback callback) throws JSONException {
        APIOperation.getInstance().registerNewStudent(txtNIC, txtName, txtEmail, txtPassword, callback);
    }

    public void getStudentVerification(String nic, APIOperation.OnAPIResultCallback callback) throws JSONException {
        APIOperation.getInstance().getStudentVerification(nic, callback);
    }

    public void getAllCategoriesList(APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().getAllCategoriesList(callback);
    }

    public void updateSocialURL(String nic, String studentID, String url, boolean b, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().updateSocialURL(nic, studentID, url, b, callback);
    }

    public void updatePassword(String nic, String studentID, String password, APIOperation.OnAPIResultCallback callback) throws JSONException {
        APIOperation.getInstance().updatePassword(nic, studentID, password, callback);
    }

    public void updateStudentInfo(String nic, String studentID, String aboutStudent, String studentName, String studentEmail, String qualification, String qualifiedYear, APIOperation.OnAPIResultCallback callback) throws JSONException {
        APIOperation.getInstance().updateStudentInfo(
                nic,
                studentID,
                aboutStudent,
                studentName,
                studentEmail,
                qualification,
                qualifiedYear,
                callback);
    }

    public void updateProfileImage(String nic, String studentID, byte[] toByteArray, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().updateProfileImage(nic, studentID, toByteArray, callback);
    }

    public void requestOTP(String nic, APIOperation.OnAPIResultCallback callback) {
        APIOperation.getInstance().requestOTP(nic, callback);
    }
}
