package com.devmonks.nibmjobportal.Firebase;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Calendar;

public class FirebaseOP {
    private static final FirebaseOP instance = new FirebaseOP();
    private final String TAG = "";
    private Context context;
    private StorageReference storageReference;
    private FirebaseStorage firebaseStorage;

    private FirebaseOP(){
        this.firebaseStorage = FirebaseStorage.getInstance();
    }

    public static FirebaseOP getInstance(){
        return instance;
    }

    public void uploadProfileImage(String fileName, byte[] imageData, FirebaseOPListener callback) {
        storageReference = firebaseStorage.getReference().child("student").child("profile_images").child(fileName);
        final UploadTask uploadTask = storageReference.putBytes(imageData);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                FirebaseCrashlytics.getInstance().recordException(e);
                callback.onFirebaseOperationFailed("Could not upload image. Please try again!");
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                taskSnapshot.getStorage().getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        callback.onImageUploaded(uri.toString());
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        e.printStackTrace();
                        FirebaseCrashlytics.getInstance().recordException(e);
                        callback.onFirebaseOperationFailed("Could not upload image. Please try again!");
                    }
                });
            }
        });
    }

    public void uploadCV(String fileName, Uri file, FirebaseOPListener callback){
        storageReference = firebaseStorage.getReference().child("student").child("student_cv").child(fileName);
        final UploadTask uploadTask = storageReference.putFile(file);
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    Log.e(TAG, "Could not complete CV upload task!");
                    callback.onFirebaseOperationFailed("Could not upload CV. Please try again!");
                    return null;
                }
                return storageReference.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (!task.isSuccessful()) {
                    Log.e(TAG, "Could not complete CV upload task!");
                    callback.onFirebaseOperationFailed("Could not upload CV. Please try again!");
                    return;
                }

                if(task.getResult()!=null)
                    callback.onCVUploaded(task.getResult().toString());
                else
                    callback.onCVUploaded(null);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                FirebaseCrashlytics.getInstance().recordException(e);
                callback.onFirebaseOperationFailed("Could not upload CV. Please try again!");
            }
        });
    }

    public void removeCV(String fileURL, FirebaseOPListener callback) {
        storageReference = firebaseStorage.getReferenceFromUrl(fileURL);
        storageReference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void unused) {
                callback.onCVRemoved();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                e.printStackTrace();
                FirebaseCrashlytics.getInstance().recordException(e);
                callback.onFirebaseOperationFailed("Could not remove CV. Please try again!");
            }
        });
    }

    public interface FirebaseOPListener {
        default void onImageUploaded(String url){
        }
        default void onCVUploaded(String url){
        }
        default void onCVRemoved(){
        }
        default void onFileRemoved(String fileName){
        }
        void onFirebaseOperationFailed(String message);
    }
}
