package com.devmonks.nibmjobportal.Activity.Auth;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import com.crowdfire.cfalertdialog.CFAlertDialog;
import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.APIService.APIModel.RegisterModel;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.Validator;
import com.devmonks.nibmjobportal.databinding.ActivityRegisterBinding;

import org.json.JSONException;

import spencerstudios.com.bungeelib.Bungee;

public class RegisterActivity extends BaseActivity implements APIOperation.OnAPIResultCallback {

    private ActivityRegisterBinding binding;
    private CFAlertDialog.Builder termsConditionPopup;
    private Dialog signUpProgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRegisterBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setupResources();
        initView();
    }

    private void setupResources() {
        signUpProgDialog = UIUtil.getProgress(RegisterActivity.this);
        binding.txtNIC.setText(getIntent().getStringExtra("NIC"));
        termsConditionPopup = new CFAlertDialog.Builder(this)
                .setDialogStyle(CFAlertDialog.CFAlertStyle.ALERT)
                .setTitle("NIBM Terms of Service")
                .setMessage(R.string.terms_conditions)
                .addButton("Cancel", -1, -1, CFAlertDialog.CFAlertActionStyle.DEFAULT, CFAlertDialog.CFAlertActionAlignment.CENTER, (dialog, which) -> {
                    dialog.dismiss();
                });
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        binding.getRoot().setOnTouchListener((v, event) -> {
            hideSoftKeyboard(RegisterActivity.this, v);
            return true;
        });

        binding.btnSignUP.setOnClickListener(v -> {
            binding.txtNIC.setError(null);
            binding.txtName.setError(null);
            binding.txtEmail.setError(null);
            binding.txtPassword.setError(null);
            binding.txtConfirmPassword.setError(null);

            if (!Validator.isValidNIC(binding.txtNIC.getText().toString())) {
                binding.txtNIC.setError("Enter a valid nic");
                vibrateDevice();
                return;
            }

            if (!Validator.isValidName(binding.txtName.getText().toString())) {
                binding.txtName.setError("Enter a valid name between 5-50 characters.");
                vibrateDevice();
                return;
            }

            if (!Validator.isValidEmail(binding.txtEmail.getText().toString())) {
                binding.txtEmail.setError("Enter a valid email address.");
                vibrateDevice();
                return;
            }

            if (!Validator.isValidPassword(binding.txtPassword.getText().toString())) {
                binding.txtPassword.setError("Password should contain both uppercase, lowercase & special characters between 6-20 digits.");
                vibrateDevice();
                return;
            }

            if (!binding.txtPassword.getText().toString().equals(binding.txtConfirmPassword.getText().toString())) {
                binding.txtPassword.setError("Passwords does not match");
                binding.txtConfirmPassword.setError("Passwords does not match");
                vibrateDevice();
                return;
            }

            if (!binding.chkTerms.isChecked()) {
                displayAlert(this, AlertType.WARNING, "Terms & Conditions", "You must agree with NIBM the Terms & Conditions.");
                return;
            }

            try {
                userSignUp(binding.txtNIC.getText().toString(), binding.txtName.getText().toString(), binding.txtEmail.getText().toString(), binding.txtPassword.getText().toString());
            } catch (JSONException gg) {

            }
        });

        binding.btnTermsConditions.setOnClickListener(v -> {
            termsConditionPopup.show();
        });

    }

    private void userSignUp(String txtNIC, String txtName, String txtEmail, String txtPassword) throws JSONException {
        signUpProgDialog.show();
//        apiOperation.registerNewStudent(txtNIC, txtName, txtEmail, txtPassword, this);
        new UserModel().registerNewStudent(txtNIC, txtName, txtEmail, txtPassword, this);
    }

    @Override
    public void onStudentRegister(RegisterModel model) {
        signUpProgDialog.dismiss();
        startActivity(new Intent(this, LoginActivity.class).putExtra("showSuccessReg", true));
        Bungee.fade(this);
        finishAffinity();
    }

    @Override
    public void onOperationFailed(String error) {
        signUpProgDialog.dismiss();
        displayAlert(this, AlertType.ERROR, "Signup Error", error);
    }

    @Override
    public void onConnectionLost(String message) {
        signUpProgDialog.dismiss();
        displayAlert(this, AlertType.WARNING, "Connection Error", message);
    }
}