package com.devmonks.nibmjobportal.Activity.JobVacancy;

import androidx.core.content.ContextCompat;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Activity.Profile.UpdateProfileActivity;
import com.devmonks.nibmjobportal.Model.JobVacancyModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.SingleOnClickListener;
import com.devmonks.nibmjobportal.databinding.ActivityViewJobBinding;
import com.hishd.lightpopup.LightPopup;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import es.dmoral.toasty.Toasty;
import spencerstudios.com.bungeelib.Bungee;

public class ViewJobActivity extends BaseActivity implements APIOperation.OnAPIResultCallback {

    private ActivityViewJobBinding binding;
    private JobVacancyModel jobVacancyModel;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityViewJobBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setupResources();
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getJobStatus();
    }

    private void initView() {
        binding.btnViewFlyer.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                startActivity(new Intent(ViewJobActivity.this, ViewFlyerActivity.class).putExtra("URL", jobVacancyModel.getJobFlyer()));
            }
        });

        binding.imgFlyer.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                startActivity(new Intent(ViewJobActivity.this, ViewFlyerActivity.class).putExtra("URL", jobVacancyModel.getJobFlyer()));
            }
        });

        binding.containerLogo.setOnClickListener(v -> {

        });

        binding.btnApplyNow.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                if(jobVacancyModel == null) {
                    displayAlert(ViewJobActivity.this, AlertType.ERROR, "Error", "Could not apply for job. Please try again!");
                    return;
                }

                if(appConfig.getUserConfig().getSkills() == null || appConfig.getUserConfig().getSkills().size() == 0 || appConfig.getUserConfig().getHigst_ql() == null) {
                    new LightPopup(ViewJobActivity.this)
                            .createDualActionDialog()
                            .setCancelledOnOutside(false)
                            .setTitle("Missing Information")
                            .setMessage("To apply a job you must complete your profile with adding your skills & highest qualification information.")
                            .setBtn1Caption("Cancel")
                            .setBtn1Color(R.color.red)
                            .setBtn1Action(Dialog::dismiss)
                            .setBtn2Caption("Update")
                            .setBtn2Color(R.color.blue_light)
                            .setBtn2Action(d -> {
                                d.dismiss();
                                startActivity(new Intent(ViewJobActivity.this, UpdateProfileActivity.class));
                                Bungee.fade(ViewJobActivity.this);
                            }).show();
                    return;
                }

                startActivity(new Intent(ViewJobActivity.this, ApplyJobActivity.class).putExtra("JOB_DATA", jobVacancyModel));
                Bungee.fade(ViewJobActivity.this);
            }
        });
    }

    private void setupResources() {
        progressDialog = UIUtil.getProgress(this);
        if(getIntent().getSerializableExtra("JOB_DATA") != null) {
            this.jobVacancyModel = (JobVacancyModel) getIntent().getSerializableExtra("JOB_DATA");
            Picasso.get().load(jobVacancyModel.getCompanyLogo()).placeholder(R.drawable.logo_placeholder).error(R.drawable.img_err).into(binding.imgCompanyLogo);
            Picasso.get().load(jobVacancyModel.getJobFlyer()).placeholder(R.color.light_gray).into(binding.imgFlyer);
            binding.txtVacancyName.setText(jobVacancyModel.getPosition());
            binding.txtCompanyName.setText(jobVacancyModel.getCompanyName());
            binding.txtClosingDate.setText(String.format(Locale.ENGLISH, "Closing Date : %s", jobVacancyModel.getClosingDate().substring(0, 10).replace("-", ".")));
            binding.txtAboutJob.setText(jobVacancyModel.getDescription());
        }
    }

    private void getJobStatus() {
        if (this.jobVacancyModel != null && appConfig.getUserConfig().getNic() != null) {
            progressDialog.show();
//            apiOperation.checkJobStatus(appConfig.getUserConfig().getNic(), this.jobVacancyModel.getJob_id(), this);
            new JobVacancyModel().checkJobStatus(appConfig.getUserConfig().getNic(), appConfig.getUserConfig().getId(), this.jobVacancyModel.getId(), this);
        }
    }

    @Override
    public void onJobStatusLoaded(boolean applied) {
        progressDialog.dismiss();
        if(applied) {
            binding.btnApplyNow.setBackground(ContextCompat.getDrawable(this, R.drawable.button_dark_gray_round));
            binding.btnApplyNow.setText("Already Applied");
            binding.btnApplyNow.setEnabled(false);
        } else {
            binding.btnApplyNow.setBackground(ContextCompat.getDrawable(this, R.drawable.button_light_blue_round));
            binding.btnApplyNow.setText("Apply Now");
            binding.btnApplyNow.setEnabled(true);
        }
    }

    @Override
    public void onOperationFailed(String error) {
        progressDialog.dismiss();
        displayAlert(this, BaseActivity.AlertType.ERROR, "Error", error);
    }

    @Override
    public void onConnectionLost(String message) {
        progressDialog.dismiss();
        Toasty.error(this, message).show();
    }
}