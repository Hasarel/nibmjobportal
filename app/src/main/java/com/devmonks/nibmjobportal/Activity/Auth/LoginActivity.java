package com.devmonks.nibmjobportal.Activity.Auth;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;

import androidx.core.app.ActivityOptionsCompat;
import androidx.core.content.ContextCompat;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Activity.HomeActivity;
import com.devmonks.nibmjobportal.Activity.ResetPassword.ResetPassEnterNicActivity;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.Validator;
import com.devmonks.nibmjobportal.databinding.ActivityLoginBinding;

import org.json.JSONException;

import spencerstudios.com.bungeelib.Bungee;

public class LoginActivity extends BaseActivity implements APIOperation.OnAPIResultCallback {

    private ActivityLoginBinding binding;
    private Dialog signInProgDialog;
    private boolean visibilityToggle = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setupResources();
        initView();
    }

    private void setupResources() {
        if (getIntent().getBooleanExtra("showSuccessReg", false)) {
            displayAlert(this, AlertType.SUCCESS, "Signup", "Student Sign-up successful. Please use your credentials to Sign-in");
        }
        signInProgDialog = UIUtil.getProgress(LoginActivity.this);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        binding.getRoot().setOnTouchListener((v, event) -> {
            hideSoftKeyboard(LoginActivity.this, v);
            return true;
        });

        binding.imgToggle.setOnClickListener(v -> {
            if(this.visibilityToggle) {
                binding.txtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                binding.imgToggle.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_visibility_off));
            } else {
                binding.txtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                binding.imgToggle.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_visibility_on));
            }

            binding.txtPassword.setSelection(binding.txtPassword.getText().length());
            this.visibilityToggle = !this.visibilityToggle;
        });

        binding.btnSignIn.setOnClickListener(v -> {
            binding.txtNIC.setError(null);
            binding.txtPassword.setError(null);
            if (!Validator.isValidNIC(binding.txtNIC.getText().toString())) {
                binding.txtNIC.setError("Enter a valid NIC Number");
                vibrateDevice();
                return;
            }

            if (!Validator.isValidPassword(binding.txtPassword.getText().toString())) {
                binding.txtPassword.setError("Invalid password");
                vibrateDevice();
//                binding.txtPassword.setError("Password should contain both uppercase, lowercase & special characters between 6-20 digits.");
                return;
            }

            try {
                userSignIn(binding.txtNIC.getText().toString(), binding.txtPassword.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        binding.btnSignUp.setOnClickListener(v -> {
            final ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, binding.imgLogo, "img_logo");
            startActivity(new Intent(this, ValidateNICActivity.class), activityOptionsCompat.toBundle());
            Bungee.fade(this);
        });

        binding.btnForgotPassword.setOnClickListener(v -> {
            startActivity(new Intent(this, ResetPassEnterNicActivity.class));
            Bungee.fade(this);
        });
    }

    private void userSignIn(String mEtUserNic, String mEtPassword) throws JSONException {
        signInProgDialog.show();
//        apiOperation.postUserLogin(mEtUserNic, mEtPassword, this);
        new UserModel().userSignIn(mEtUserNic, mEtPassword, this);
    }

    @Override
    public void onLoginResponse(UserModel loginModel) {
        signInProgDialog.dismiss();
        appConfig.saveUserConfig(loginModel);
        startActivity(new Intent(this, HomeActivity.class));
        Bungee.fade(this);
        finishAffinity();
    }

    @Override
    public void onOperationFailed(String error) {
        signInProgDialog.dismiss();
        displayAlert(this, AlertType.ERROR, "SignIn Error", error);
    }

    @Override
    public void onConnectionLost(String message) {
        signInProgDialog.dismiss();
        displayAlert(this, AlertType.WARNING, "Connection Error", message);
    }
}