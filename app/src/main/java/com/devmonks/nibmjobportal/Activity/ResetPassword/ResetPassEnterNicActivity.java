package com.devmonks.nibmjobportal.Activity.ResetPassword;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.Util.Validator;
import com.devmonks.nibmjobportal.databinding.ActivityResetPassEnterNicBinding;

import spencerstudios.com.bungeelib.Bungee;

public class ResetPassEnterNicActivity extends BaseActivity implements APIOperation.OnAPIResultCallback {

    private ActivityResetPassEnterNicBinding binding;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityResetPassEnterNicBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initView();
        setupResources();
    }

    private void setupResources() {
        progressDialog = UIUtil.getProgress(this);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        binding.getRoot().setOnTouchListener((v, event) -> {
            hideSoftKeyboard(this, v);
            return true;
        });

        binding.btnSignIn.setOnClickListener(v -> {
            onBackPressed();
        });

        binding.btnReset.setOnClickListener(v -> {
            if (!Validator.isValidNIC(binding.txtNIC.getText().toString())) {
                binding.txtNIC.setError("Enter a valid NIC Number");
                vibrateDevice();
                return;
            }

            progressDialog.show();
//            apiOperation.requestOTP(binding.txtNIC.getText().toString(), this);
            new UserModel().requestOTP(binding.txtNIC.getText().toString(), this);
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.animationView.playAnimation();
    }

    @Override
    public void onResetOtpReceived(String otp, String email, String studentID) {
        progressDialog.dismiss();
        binding.animationView.cancelAnimation();
        startActivity(new Intent(this, ResetPassEnterOtpActivity.class)
                .putExtra("EMAIL", email)
                .putExtra("OTP", otp)
                .putExtra("STUDENT_ID", studentID)
                .putExtra("NIC", binding.txtNIC.getText().toString())
        );
        Bungee.fade(this);
    }

    @Override
    public void onOperationFailed(String error) {
        progressDialog.dismiss();
        displayAlert(this, AlertType.ERROR, "Error!", error);
    }

    @Override
    public void onConnectionLost(String message) {
        progressDialog.dismiss();
        displayAlert(this, AlertType.WARNING, "Connection Error", message);
    }
}