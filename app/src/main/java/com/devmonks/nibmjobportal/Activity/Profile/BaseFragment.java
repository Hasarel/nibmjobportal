package com.devmonks.nibmjobportal.Activity.Profile;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Application.MainApplication;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.AppConfig;
import com.devmonks.nibmjobportal.Util.UIUtil;

import org.aviran.cookiebar2.CookieBar;

public abstract class BaseFragment extends Fragment {
    protected Vibrator vibrator;
    protected APIOperation apiOperation;
    protected AppConfig appConfig;
    protected final UIUtil UIUtil = new UIUtil();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        vibrator = (Vibrator) MainApplication.getContext().getSystemService(Context.VIBRATOR_SERVICE);
        apiOperation = APIOperation.getInstance();
        appConfig = AppConfig.getInstance();
    }

    protected static void hideSoftKeyboard (Activity activity, View view)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    protected void vibrateDevice(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            vibrator.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
        } else {
            vibrator.vibrate(50);
        }
    }

    protected void displayAlert(Activity activity, BaseActivity.AlertType type, String title, String message) {
        switch (type) {
            case SUCCESS:
                CookieBar.build(activity)
                        .setTitle(title)
                        .setMessage(message)
                        .setCookiePosition(CookieBar.BOTTOM)
                        .setBackgroundColor(R.color.bg_success)
                        .setIconAnimation(R.animator.fade)
                        .setIcon(R.drawable.ic_success)
                        .setDuration(3000)
                        .show();
                break;
            case WARNING:
                CookieBar.build(activity)
                        .setTitle(title)
                        .setMessage(message)
                        .setCookiePosition(CookieBar.BOTTOM)
                        .setBackgroundColor(R.color.bg_warning)
                        .setIconAnimation(R.animator.fade)
                        .setIcon(R.drawable.ic_warning)
                        .setDuration(3000)
                        .show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(50, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(50);
                }
                break;
            case ERROR:
                CookieBar.build(activity)
                        .setTitle(title)
                        .setMessage(message)
                        .setCookiePosition(CookieBar.BOTTOM)
                        .setBackgroundColor(R.color.bg_error)
                        .setIcon(R.drawable.ic_error)
                        .setIconAnimation(R.animator.fade)
                        .setDuration(3000)
                        .show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    vibrator.vibrate(VibrationEffect.createOneShot(100, VibrationEffect.DEFAULT_AMPLITUDE));
                } else {
                    vibrator.vibrate(100);
                }
                break;
        }
    }

    public enum AlertType {
        SUCCESS,
        WARNING,
        ERROR
    }
}
