package com.devmonks.nibmjobportal.Activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.JobVacancy.ViewJobActivity;
import com.devmonks.nibmjobportal.Activity.Profile.ProfileActivity;
import com.devmonks.nibmjobportal.Adapters.JOBVacancyAdapter;
import com.devmonks.nibmjobportal.Adapters.SelectedJobCategoryAdapter;
import com.devmonks.nibmjobportal.Model.JobCategoryModel;
import com.devmonks.nibmjobportal.Model.JobVacancyModel;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.FadeAnimator;
import com.devmonks.nibmjobportal.Util.ItemOffsetDecoration;
import com.devmonks.nibmjobportal.Util.SingleOnClickListener;
import com.devmonks.nibmjobportal.databinding.ActivityHomeBinding;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import spencerstudios.com.bungeelib.Bungee;

public class HomeActivity extends BaseActivity implements JOBVacancyAdapter.OnJobVacancyClickedListener, APIOperation.OnAPIResultCallback, SelectedJobCategoryAdapter.OnJobCategoryClickedListener {

    private ActivityHomeBinding binding;
    private SelectedJobCategoryAdapter mSelectedJobCategoryAdapter;
    private JOBVacancyAdapter mJobVacancyAdapter;
    private Animation recyclerAnimation;
    private ArrayList<JobVacancyModel> mJOBVacancyModelModelArrayList = new ArrayList<>();
    private ArrayList<JobCategoryModel> mJobCategoryModelArrayList = new ArrayList<>();
    ArrayList<JobVacancyModel> filteredJobs = new ArrayList<>();
    private Dialog categoryProgressDialog;
    private Dialog jobsProgressDialog;
    private UserModel userData = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        setupResources();
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(appConfig.getDataUpdatedStatus()) {
            appConfig.setDataUpdatedStatus(false);
            refreshUI();
        }
    }

    private void setupResources() {
        recyclerAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_enter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        final ItemOffsetDecoration categoryItemDecoration = new ItemOffsetDecoration(this, R.dimen._1sdp, R.dimen._1sdp, R.dimen._5sdp, R.dimen._5sdp);
        final ItemOffsetDecoration vacancyItemDecoration = new ItemOffsetDecoration(this, R.dimen._5sdp, R.dimen._8sdp, R.dimen._1sdp, R.dimen._1sdp);
        binding.recyclerMyCategories.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerMyCategories.addItemDecoration(categoryItemDecoration);
        binding.recyclerMyCategories.setLayoutAnimation(controller);
        binding.recyclerJobs.setLayoutManager(new LinearLayoutManager(this));
        binding.recyclerJobs.addItemDecoration(vacancyItemDecoration);
        binding.recyclerJobs.setLayoutAnimation(controller);

        //Preparing re-usable adapter resources
        mJobVacancyAdapter = new JOBVacancyAdapter(mJOBVacancyModelModelArrayList, this, this);
        binding.recyclerJobs.setAdapter(mJobVacancyAdapter);

        //Preparing re-usable adapter resources
        mSelectedJobCategoryAdapter = new SelectedJobCategoryAdapter(mJobCategoryModelArrayList, this, this);
        binding.recyclerMyCategories.setAdapter(mSelectedJobCategoryAdapter);

        //Preparing Progress overlay dialog
        categoryProgressDialog = UIUtil.getProgress(HomeActivity.this);
        jobsProgressDialog = UIUtil.getProgress(HomeActivity.this);

        //Initial load of Categories and Job Posts
        refreshUI();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        binding.getRoot().setOnTouchListener((v, event) -> {
            hideSoftKeyboard(this, v);
            return true;
        });

        binding.imgProfile.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class));
            }
        });

        binding.btnAddCategory.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                startActivity(new Intent(HomeActivity.this, ProfileActivity.class).putExtra("FROM_ADD_CATEGORY", true));
            }
        });

        binding.activityHomeEtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (!binding.activityHomeEtSearch.hasFocus())
                    return;
                jobListSearchByName(editable.toString());
                setAnimationStatus(false);
            }
        });

        binding.activityHomeEtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                binding.activityHomeEtSearch.clearFocus();
                hideSoftKeyboard(HomeActivity.this, v);
                return true;
            }
        });

        binding.containerRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                binding.activityHomeEtSearch.clearFocus();
                binding.activityHomeEtSearch.setText(null);
                binding.containerRefresh.setRefreshing(false);
                refreshUI();
            }
        });
    }

    private void refreshUI() {
        userData = appConfig.getUserConfig();
        if (userData == null)
            return;
        if (userData.getName() != null) {
            if (userData.getName().length() > 20)
                binding.txtStudentName.setText(userData.getName().split(" ")[0]);
            else
                binding.txtStudentName.setText(userData.getName());
        }
        if (userData.getImage_url() != null)
            Picasso.get().load(userData.getImage_url()).placeholder(R.drawable.img_avatar).error(R.drawable.img_avatar).into(binding.imgProfile);
        if (userData.getNic() != null) {
            getAllJobPostFromAPI(userData.getNic());
            getJobCategoryList(userData.getNic());
        }
    }

    private void getAllJobPostFromAPI(String nic) {
        jobsProgressDialog.show();
//        apiOperation.getJobPostsFromAPI(nic, this);
        new JobVacancyModel().getJobPostsFromAPI(nic, userData.getId(), this);
    }

    private void getJobCategoryList(String nic) {
        categoryProgressDialog.show();
//        apiOperation.getSelectedJobCategoryList(nic, this);
        new JobCategoryModel().getSelectedJobCategoryList(nic, userData.getId(), this);
    }

    private void getJobPostByCategory(String categoryID) {
        jobsProgressDialog.show();
//        apiOperation.getJobsBasedOnCategory(category, this);
        new JobVacancyModel().getJobsBasedOnCategory(categoryID, this);
    }

    private void jobListSearchByName(String SearchText) {
        filteredJobs.clear();
        for (JobVacancyModel items : mJOBVacancyModelModelArrayList) {
            if (items.getPosition().toLowerCase().contains(SearchText.toLowerCase())) {
                filteredJobs.add(items);
            }
        }
        mJobVacancyAdapter.setJobVacancyList(filteredJobs);
        binding.recyclerJobs.startAnimation(recyclerAnimation);
    }

    private void setAnimationStatus(boolean status) {
        if (status) {
//            binding.recyclerJobs.setVisibility(View.INVISIBLE);
            FadeAnimator.fadeOutAnimation(binding.recyclerJobs);
            binding.animationView.setVisibility(View.VISIBLE);
//            FadeAnimator.fadeInAnimation(binding.animationView);
            binding.animationView.playAnimation();
        } else {
//            FadeAnimator.fadeOutAnimation(binding.animationView);
            binding.animationView.setVisibility(View.INVISIBLE);
            binding.animationView.cancelAnimation();
            FadeAnimator.fadeInAnimation(binding.recyclerJobs);
//            binding.recyclerJobs.setVisibility(View.VISIBLE);
        }
    }

    private void setConnectionLostStatus(boolean status) {
        if (status) {
            binding.txtConnectionLost.setVisibility(View.VISIBLE);
            binding.recyclerMyCategories.setVisibility(View.INVISIBLE);
        } else {
            binding.txtConnectionLost.setVisibility(View.INVISIBLE);
            binding.recyclerMyCategories.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onJobVacancySelected(int position, JobVacancyModel data) {
        startActivity(new Intent(this, ViewJobActivity.class).putExtra("JOB_DATA", data));
        Bungee.fade(this);
    }

    @Override
    public void onJobCategorySelected(int position, JobCategoryModel data) {
        binding.activityHomeEtSearch.clearFocus();
        binding.activityHomeEtSearch.setText(null);
        //Get all jobs
        if (position == 0) {
            if (userData.getNic() != null)
                getAllJobPostFromAPI(userData.getNic());
            return;
        }
        if (data.getJob_count() == 0) {
            setAnimationStatus(true);
            return;
        } else {
            setAnimationStatus(false);
        }
        getJobPostByCategory(data.getId());
    }

    @Override
    public void onJobPostsLoaded(ArrayList<JobVacancyModel> jobPosts) {
        jobsProgressDialog.dismiss();
        setConnectionLostStatus(false);
        if (jobPosts.size() == 0) {
            setAnimationStatus(true);
            return;
        } else {
            setAnimationStatus(false);
        }
        mJOBVacancyModelModelArrayList.clear();
        mJOBVacancyModelModelArrayList.addAll(jobPosts);
        mJobVacancyAdapter.setJobVacancyList(mJOBVacancyModelModelArrayList);
        binding.recyclerJobs.startAnimation(recyclerAnimation);
    }

    @Override
    public void onJobCategoriesLoaded(ArrayList<JobCategoryModel> jobCategories) {
        categoryProgressDialog.dismiss();
        setConnectionLostStatus(false);
        if (jobCategories.size() == 0) {
            binding.btnAddCategory.setVisibility(View.VISIBLE);
            binding.recyclerMyCategories.setVisibility(View.INVISIBLE);
            binding.txtAddCategoriesMessage.setVisibility(View.VISIBLE);
            return;
        } else {
            binding.btnAddCategory.setVisibility(View.INVISIBLE);
            binding.txtAddCategoriesMessage.setVisibility(View.INVISIBLE);
            binding.recyclerMyCategories.setVisibility(View.VISIBLE);
        }

        mJobCategoryModelArrayList.clear();
        mJobCategoryModelArrayList.add(new JobCategoryModel("0", "All Job Vacancies", 0));

        mJobCategoryModelArrayList.addAll(jobCategories);
        mSelectedJobCategoryAdapter.notifyDataSetChanged();
        binding.recyclerMyCategories.startAnimation(recyclerAnimation);
    }

    @Override
    public void onFilteredJobsLoaded(ArrayList<JobVacancyModel> jobPosts) {
        jobsProgressDialog.dismiss();
        setConnectionLostStatus(false);
        if (jobPosts.size() == 0) {
            setAnimationStatus(true);
            return;
        } else {
            setAnimationStatus(false);
        }
        mJobVacancyAdapter.setJobVacancyList(jobPosts);
        binding.recyclerJobs.startAnimation(recyclerAnimation);
    }

    @Override
    public void onOperationFailed(String error) {
        jobsProgressDialog.dismiss();
        categoryProgressDialog.dismiss();
        displayAlert(this, AlertType.ERROR, "Error", error);
    }

    @Override
    public void onConnectionLost(String message) {
        jobsProgressDialog.dismiss();
        categoryProgressDialog.dismiss();
        setConnectionLostStatus(true);
        Toasty.error(this, message).show();
    }
}