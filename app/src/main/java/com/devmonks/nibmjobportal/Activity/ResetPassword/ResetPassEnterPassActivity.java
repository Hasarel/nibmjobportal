package com.devmonks.nibmjobportal.Activity.ResetPassword;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.devmonks.nibmjobportal.APIService.APIModel.PasswordUpdateModel;
import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.Auth.LoginActivity;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.Util.Validator;
import com.devmonks.nibmjobportal.databinding.ActivityResetPassEnterPassBinding;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import org.json.JSONException;

public class ResetPassEnterPassActivity extends BaseActivity implements APIOperation.OnAPIResultCallback {

    private ActivityResetPassEnterPassBinding binding;
    private String nic = "";
    private String studentID = "";
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityResetPassEnterPassBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initView();
        setupResources();
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.animationView.playAnimation();
    }

    private void setupResources() {
        progressDialog = UIUtil.getProgress(this);

        if(getIntent().hasExtra("NIC") && getIntent().hasExtra("STUDENT_ID")) {
            this.nic = getIntent().getStringExtra("NIC");
            this.studentID = getIntent().getStringExtra("STUDENT_ID");
        } else {
            displayAlert(this, AlertType.ERROR, "Operation Failed", "Could reset password, Please try again!");
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        binding.getRoot().setOnTouchListener((v, event) -> {
            hideSoftKeyboard(this, v);
            return true;
        });

        binding.btnChange.setOnClickListener(v -> {
            //Check if the OTP does not contain in extras
            if(this.nic.length() == 0) {
                displayAlert(this, AlertType.ERROR, "Operation Failed", "Could not change password, Please try again!");
                return;
            }

            if (!Validator.isValidPassword(binding.txtPassword.getText().toString())) {
                binding.txtPassword.setError("Password should contain both uppercase, lowercase & special characters between 6-20 digits.");
                vibrateDevice();
                return;
            }

            if (!binding.txtPassword.getText().toString().equals(binding.txtConfirmPassword.getText().toString())) {
                binding.txtPassword.setError("Passwords does not match");
                binding.txtConfirmPassword.setError("Passwords does not match");
                vibrateDevice();
                return;
            }

            try {
                progressDialog.show();
//                apiOperation.updatePassword(this.nic, binding.txtPassword.getText().toString(), this);
                new UserModel().updatePassword(this.nic, this.studentID, binding.txtPassword.getText().toString(), this);
            } catch (JSONException e) {
                e.printStackTrace();
                FirebaseCrashlytics.getInstance().recordException(e);
            }
        });
    }

    @Override
    public void onPasswordUpdated(PasswordUpdateModel result) {
        progressDialog.dismiss();
        displayAlert(this, AlertType.SUCCESS, "Password Changed!", "Passwords changed successfully.", dismissType -> {
            startActivity(new Intent(ResetPassEnterPassActivity.this, LoginActivity.class));
            finishAffinity();
        });
    }

    @Override
    public void onOperationFailed(String error) {
        progressDialog.dismiss();
        displayAlert(this, AlertType.ERROR, "Error!", error);
    }

    @Override
    public void onConnectionLost(String message) {
        progressDialog.dismiss();
        displayAlert(this, AlertType.WARNING, "Connection Error", message);
    }
}