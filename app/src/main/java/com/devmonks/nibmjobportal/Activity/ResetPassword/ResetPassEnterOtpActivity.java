package com.devmonks.nibmjobportal.Activity.ResetPassword;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.Util.Validator;
import com.devmonks.nibmjobportal.databinding.ActivityResetPassEnterOtpBinding;

import java.util.Locale;

import spencerstudios.com.bungeelib.Bungee;

public class ResetPassEnterOtpActivity extends BaseActivity implements APIOperation.OnAPIResultCallback {

    private ActivityResetPassEnterOtpBinding binding;
    private CountDownTimer countDownTimer;
    private String otp = "0000";
    private String email = "";
    private String nic = "";
    private String studentID = "";
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityResetPassEnterOtpBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initView();
        setupResources();
    }

    @Override
    protected void onResume() {
        super.onResume();
        binding.animationView.playAnimation();
    }

    private void setupResources() {
        progressDialog = UIUtil.getProgress(this);
        countDownTimer = new CountDownTimer(30000,1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                binding.txtCountSecond.setText(String.format(Locale.ENGLISH, "%d Sec", millisUntilFinished/1000));
            }

            @Override
            public void onFinish() {
                stopCountDown();
            }
        };

        if(getIntent().hasExtra("EMAIL") && getIntent().hasExtra("OTP") && getIntent().hasExtra("STUDENT_ID")) {
            this.otp = getIntent().getStringExtra("OTP");
            this.email = getIntent().getStringExtra("EMAIL");
            this.nic = getIntent().getStringExtra("NIC");
            this.studentID = getIntent().getStringExtra("STUDENT_ID");
            String emailString = this.email.substring(0, this.email.indexOf("@"));
            emailString = emailString.substring(0, emailString.length() / 2) + "****" + this.email.substring(this.email.indexOf("@"));
            binding.textMaskedEmail.setText(emailString);
            startCountDown();
        } else {
            displayAlert(this, AlertType.ERROR, "OTP Failed", "Could not send OTP, Please try again!");
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        binding.getRoot().setOnTouchListener((v, event) -> {
            hideSoftKeyboard(this, v);
            return true;
        });

        binding.btnVerify.setOnClickListener(v -> {

            //Check if the OTP does not contain in extras
            if(this.otp.equals("0000")) {
                displayAlert(this, AlertType.ERROR, "OTP Failed", "Could not send OTP, Please try again!");
                return;
            }

            if (!Validator.isValidOTP(binding.txtOTP.getText().toString())) {
                binding.txtOTP.setError("Enter a valid OTP");
                vibrateDevice();
                return;
            }

            if(!this.otp.equals(binding.txtOTP.getText().toString())) {
                displayAlert(this, AlertType.ERROR, "Invalid OTP", "The entered OTP does not match!");
            } else {
                binding.animationView.cancelAnimation();
                startActivity(new Intent(this, ResetPassEnterPassActivity.class)
                        .putExtra("NIC", this.nic)
                        .putExtra("STUDENT_ID", this.studentID)
                );
                Bungee.fade(this);
                finish();
            }
        });
        binding.btnResendOTP.setOnClickListener(v -> {
            progressDialog.show();
//            apiOperation.requestOTP(this.nic, this);
            new UserModel().requestOTP(this.nic, this);
        });
    }

    private void startCountDown() {
        binding.btnResendOTP.setVisibility(View.INVISIBLE);
        binding.containerCountdown.setVisibility(View.VISIBLE);
        countDownTimer.start();

        displayAlert(this, AlertType.INFO, "OTP Sent", "Please check your email to find your OTP. Make sure to check 'Spam' folder.");
    }

    private void stopCountDown() {
        binding.btnResendOTP.setVisibility(View.VISIBLE);
        binding.containerCountdown.setVisibility(View.INVISIBLE);
    }

    @Override
    public void onResetOtpReceived(String otp, String email, String studentID) {
        progressDialog.dismiss();
        this.otp = otp;
        this.studentID = studentID;
        startCountDown();
    }

    @Override
    public void onOperationFailed(String error) {
        progressDialog.dismiss();
        displayAlert(this, AlertType.ERROR, "Error!", error);
    }

    @Override
    public void onConnectionLost(String message) {
        progressDialog.dismiss();
        displayAlert(this, AlertType.WARNING, "Connection Error", message);
    }
}