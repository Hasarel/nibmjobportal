package com.devmonks.nibmjobportal.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.devmonks.nibmjobportal.Activity.Auth.LoginActivity;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.databinding.ActivitySplashBinding;

import spencerstudios.com.bungeelib.Bungee;

public class SplashActivity extends BaseActivity {

    ActivitySplashBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivitySplashBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        final Animation fadeIn = AnimationUtils.loadAnimation(this, R.anim.fade_enter);
        binding.imgLogo.startAnimation(fadeIn);

        new Handler().postDelayed(() -> {
            if (appConfig.getUserConfig() != null && appConfig.getUserLogged())
                startActivity(new Intent(SplashActivity.this, HomeActivity.class));
            else
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));
            Bungee.fade(this);
            finish();

        }, 2000);
    }
}