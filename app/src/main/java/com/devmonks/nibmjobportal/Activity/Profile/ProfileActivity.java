package com.devmonks.nibmjobportal.Activity.Profile;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.databinding.ActivityProfileBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class ProfileActivity extends AppCompatActivity {
    private ActivityProfileBinding binding;
    private boolean FROM_ADD_CATEGORY = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setupResources();
    }

    private void setupResources() {

        this.FROM_ADD_CATEGORY = getIntent().getBooleanExtra("FROM_ADD_CATEGORY", false);

        final BottomNavigationView.OnNavigationItemSelectedListener bottomNavigationHandler = new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                if (item.getItemId() == R.id.about_me) {
                    getSupportFragmentManager().beginTransaction().replace(binding.container.getId(), new AboutMeFragment()).commit();
                } else if (item.getItemId() == R.id.manage_cv) {
                    getSupportFragmentManager().beginTransaction().replace(binding.container.getId(), new ManageCVFragment()).commit();
                }
//                switch (item.getItemId()){
//                    case R.id.about_me:
//                        getSupportFragmentManager().beginTransaction().replace(binding.container.getId(), new AboutMeFragment()).commit();
//                        break;
//                    case R.id.manage_cv:
//                        getSupportFragmentManager().beginTransaction().replace(binding.container.getId(), new ManageCVFragment()).commit();
//                        break;
//                }
                return true;
            }
        };

        binding.navBar.setOnNavigationItemSelectedListener(bottomNavigationHandler);
        final Bundle bundle = new Bundle();
        bundle.putBoolean("FROM_ADD_CATEGORY", FROM_ADD_CATEGORY);
        final Fragment aboutMeFragment = new AboutMeFragment();
        aboutMeFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(binding.container.getId(), aboutMeFragment).commit();
    }
}