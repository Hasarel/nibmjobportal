package com.devmonks.nibmjobportal.Activity.Profile;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.EditorInfo;

import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.Auth.LoginActivity;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Adapters.ManageJobCategoryAdapter;
import com.devmonks.nibmjobportal.Adapters.ManageSkillsAdapter;
import com.devmonks.nibmjobportal.Model.JobCategoryModel;
import com.devmonks.nibmjobportal.Model.SkillModel;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.ItemOffsetDecoration;
import com.devmonks.nibmjobportal.Util.SingleOnClickListener;
import com.devmonks.nibmjobportal.Util.Validator;
import com.devmonks.nibmjobportal.databinding.FragmentAboutMeBinding;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.hishd.animdialog.AnimDialog;
import com.hishd.multiselectpopup.MultiSelectDialog;
import com.hishd.multiselectpopup.MultiSelectModel;
import com.squareup.picasso.Picasso;

import org.json.JSONException;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;
import smartdevelop.ir.eram.showcaseviewlib.GuideView;
import smartdevelop.ir.eram.showcaseviewlib.config.DismissType;
import smartdevelop.ir.eram.showcaseviewlib.config.Gravity;
import smartdevelop.ir.eram.showcaseviewlib.config.PointerType;
import spencerstudios.com.bungeelib.Bungee;

public class AboutMeFragment extends BaseFragment implements APIOperation.OnAPIResultCallback {

    private final String TAG = "AboutMeFragment";
    private FragmentAboutMeBinding binding;
    private UserModel userData = null;
    private AnimDialog signOutDialog;
    private ArrayList<SkillModel> skillList = new ArrayList<>();
    private ArrayList<JobCategoryModel> selectedCategoryList = new ArrayList<>();
    private ArrayList<JobCategoryModel> availableCategories = new ArrayList<>();
    private Animation recyclerAnimation;
    private ManageSkillsAdapter manageSkillsAdapter;
    private ManageJobCategoryAdapter manageJobCategoryAdapter;
    private Dialog progressDialog;
    MultiSelectDialog selectJobCategoryDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAboutMeBinding.inflate(inflater, container, false);
        setupResources();
        initView();
        return binding.getRoot();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        binding.getRoot().setOnTouchListener((v, event) -> {
            hideSoftKeyboard(requireActivity(), v);
            binding.txtGithubURL.clearFocus();
            binding.txtLinkedInURL.clearFocus();
            return true;
        });

        binding.btnBack.setOnClickListener(v -> {
            requireActivity().finish();
        });

        binding.btnManageCategory.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                getAvailableCategories();
            }
        });

        binding.btnSignOut.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                signOutDialog.show();
            }
        });

        binding.btnEditProfile.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                startActivity(new Intent(requireContext(), UpdateProfileActivity.class));
                Bungee.fade(requireContext());
            }
        });

        binding.txtGithubURL.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (binding.txtGithubURL.getText().length() > 1) {
                    if (!Validator.isValidURL(binding.txtGithubURL.getText().toString())) {
                        binding.txtGithubURL.setError("Enter a valid URL!");
                        vibrateDevice();
                        return false;
                    }
                }

                binding.txtGithubURL.clearFocus();
                hideSoftKeyboard(requireActivity(), v);
                if (userData != null && userData.getNic() != null) {
                    try {
                        updateGithubURL(userData.getNic(), binding.txtGithubURL.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else
                    Toasty.error(requireContext(), "NIC not found").show();
            }
            return true;
        });
        binding.txtLinkedInURL.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                if (binding.txtLinkedInURL.getText().length() > 1) {
                    if (!Validator.isValidURL(binding.txtLinkedInURL.getText().toString())) {
                        binding.txtLinkedInURL.setError("Enter a valid URL!");
                        vibrateDevice();
                        return false;
                    }
                }

                binding.txtLinkedInURL.clearFocus();
                hideSoftKeyboard(requireActivity(), v);
                if (userData != null && userData.getNic() != null) {
                    try {
                        updateLinkedInURL(userData.getNic(), binding.txtLinkedInURL.getText().toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else
                    Toasty.error(requireContext(), "NIC not found").show();
            }
            return true;
        });
    }

    private void setupResources() {
        recyclerAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_enter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_fall_down);
        final ItemOffsetDecoration itemOffsetDecoration = new ItemOffsetDecoration(requireContext(), R.dimen._1sdp, R.dimen._1sdp, R.dimen._5sdp, R.dimen._5sdp);
        binding.recyclerMyCategories.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerMySkills.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerMyCategories.addItemDecoration(itemOffsetDecoration);
        binding.recyclerMySkills.addItemDecoration(itemOffsetDecoration);
        binding.recyclerMyCategories.setLayoutAnimation(controller);
        binding.recyclerMySkills.setLayoutAnimation(controller);

        //Preparing adapters
        manageSkillsAdapter = new ManageSkillsAdapter(this.skillList, requireContext());
        manageJobCategoryAdapter = new ManageJobCategoryAdapter(this.selectedCategoryList, requireContext());
        //Setting adapters to RecyclerViews
        binding.recyclerMyCategories.setAdapter(manageJobCategoryAdapter);
        binding.recyclerMySkills.setAdapter(manageSkillsAdapter);

        //Preparing Progress overlay dialog
        progressDialog = UIUtil.getProgress(requireActivity());

        signOutDialog = new AnimDialog(requireContext())
                .createAnimatedDualDialog()
                .setAnimation("sign_out.json")
                .setBackgroundColor(R.color.white)
                .setButton1BackgroundColor(R.color.warning_red)
                .setButton2BackgroundColor(R.color.blue_light)
                .setTitle("Sign Out")
                .setTitleColor(R.color.dark_semi)
                .setContentColor(R.color.dark_semi)
                .setContent("Sign out from the application will require you to perform a re sign-in to the application. Are you sure you want to Sign Out?")
                .setButton1("Sign out", R.color.white, false, v -> {
                    v.dismiss();
                    appConfig.clearUserConfig();
                    startActivity(new Intent(requireContext(), LoginActivity.class));
                    Bungee.fade(requireContext());
                    requireActivity().finishAffinity();
                })
                .setButton2("Cancel", R.color.white, false, Dialog::dismiss);


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            if (bundle.getBoolean("FROM_ADD_CATEGORY", false)) {
                new GuideView.Builder(requireContext())
                        .setTitle("Add Categories")
                        .setContentText("Use Manage to update preferred categories.")
                        .setGravity(Gravity.auto) //optional
                        .setDismissType(DismissType.anywhere) //optional - default DismissType.targetView
                        .setTargetView(binding.btnManageCategory)
                        .setContentTextSize(12)//optional
                        .setTitleTextSize(14)//optional
                        .setTitleTypeFace(ResourcesCompat.getFont(requireContext(), R.font.gilroy_bold))
                        .setContentTypeFace(ResourcesCompat.getFont(requireContext(), R.font.gilroy_medium))
                        .setPointerType(PointerType.arrow)
                        .build()
                        .show();
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshStudentData();
    }

    //Call to this method will load all available job categories with API call
    private void getAvailableCategories() {
        progressDialog.show();
//        apiOperation.getAllCategoriesList(this);
        new UserModel().getAllCategoriesList(this);
    }

    //Call to this method will update the Student GitHub URL with API call
    private void updateGithubURL(String nic, String url) throws JSONException {
        progressDialog.show();
//        apiOperation.updateSocialURL(nic, url, true, this);
        new UserModel().updateSocialURL(nic, userData.getId(), url, true, this);
    }

    //Call to this method will update the Student LinkedIn URL with API call
    private void updateLinkedInURL(String nic, String url) throws JSONException {
        progressDialog.show();
//        apiOperation.updateSocialURL(nic, url, false, this);
        new UserModel().updateSocialURL(nic, userData.getId(), url, false, this);
    }


    //Call to this method will update the Students preferred categories with API call
    private void updateMyCategories(ArrayList<Integer> selectedID, String nic) {
        progressDialog.show();
        final ArrayList<JobCategoryModel> categoryList = new ArrayList<>();
        for (int id : selectedID) {
            categoryList.add(this.availableCategories.get(id));
        }

//        apiOperation.updateSelectedCategories(categoryList, nic, this);
        new JobCategoryModel().updateSelectedCategories(categoryList, nic, userData.getId(), this);
    }

    //Prepare the Spinner popup
    private void prepareManageCategoriesSpinner(ArrayList<JobCategoryModel> availableCategoryList) {
        userData = appConfig.getUserConfig();

        final ArrayList<MultiSelectModel> listOfCategories = new ArrayList<>();
        final ArrayList<Integer> selectedCategoryID = new ArrayList<>();
        if (selectedCategoryList.size() == 0) {
            for (int i = 0; i < availableCategoryList.size(); i++)
                listOfCategories.add(new MultiSelectModel(i, availableCategoryList.get(i).getName()));
        } else {
            for (int i = 0; i < availableCategoryList.size(); i++) {
                for (JobCategoryModel selectedCat : selectedCategoryList) {
                    if (availableCategoryList.get(i).getName().equals(selectedCat.getName()))
                        selectedCategoryID.add(i);
                }
                listOfCategories.add(new MultiSelectModel(i, availableCategoryList.get(i).getName()));
            }
        }

        selectJobCategoryDialog = new MultiSelectDialog(requireContext())
                .setTitle("Select Categories")
                .setTitleTypeface(ResourcesCompat.getFont(requireContext(), R.font.gilroy_bold))
                .setPositiveText("Done")
                .setNegativeText("Cancel")
                .setMinSelectionLimit(0)
                .setMaxSelectionLimit(availableCategoryList.size())
                .preSelectIDsList(selectedCategoryID)
                .multiSelectList(listOfCategories)
                .build()
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String s) {
                        if (userData == null)
                            return;
                        if (userData.getNic() != null)
                            updateMyCategories(selectedIds, userData.getNic());
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "Category Selection cancelled");
                    }
                });

        try {
            selectJobCategoryDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    private void refreshStudentData() {
        userData = appConfig.getUserConfig();
        if (userData == null)
            return;
        if (userData.getImage_url() != null)
            Picasso.get().load(userData.getImage_url()).placeholder(R.drawable.img_avatar).error(R.drawable.img_avatar).into(binding.imgProfile);
        if (userData.getAbout() != null)
            binding.txtAboutStudent.setText(userData.getAbout());
        else
            binding.txtAboutStudent.setText("Add an introduction about yourself.");

        binding.txtStudentName.setText(userData.getName());
        binding.txtStudentEmail.setText(userData.getEmail());
        binding.txtStudentNIC.setText(userData.getNic());
        binding.txtLinkedInURL.setText(userData.getLikedin_url());
        binding.txtGithubURL.setText(userData.getGit_url());

        if (userData.getSkills() != null && userData.getSkills().size() != 0) {
            this.skillList.clear();
            this.skillList.addAll(userData.getSkills());
            this.manageSkillsAdapter.notifyDataSetChanged();
            binding.recyclerMySkills.startAnimation(recyclerAnimation);
            binding.txtAddSkillsMessage.setVisibility(View.INVISIBLE);
            binding.recyclerMySkills.setVisibility(View.VISIBLE);
        } else {
            binding.txtAddSkillsMessage.setVisibility(View.VISIBLE);
            binding.recyclerMySkills.setVisibility(View.INVISIBLE);
        }

        if (userData.getCategories() != null && userData.getCategories().size() != 0) {
            this.selectedCategoryList.clear();
            this.selectedCategoryList.addAll(userData.getCategories());
            this.manageJobCategoryAdapter.notifyDataSetChanged();
            binding.recyclerMyCategories.startAnimation(recyclerAnimation);
            binding.txtAddCategoriesMessage.setVisibility(View.INVISIBLE);
            binding.recyclerMyCategories.setVisibility(View.VISIBLE);
        } else {
            binding.txtAddCategoriesMessage.setVisibility(View.VISIBLE);
            binding.recyclerMyCategories.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onJobCategoriesLoaded(ArrayList<JobCategoryModel> jobCategories) {
        progressDialog.dismiss();
        if (jobCategories.size() == 0) {
            Toasty.info(requireContext(), "No available categories found").show();
            return;
        }
        this.availableCategories.clear();
        this.availableCategories.addAll(jobCategories);
        prepareManageCategoriesSpinner(jobCategories);
    }

    @Override
    public void onSocialURLUpdated(String url, boolean isGitHubURL) {
        progressDialog.dismiss();
        if (isGitHubURL) {
//            displayAlert(requireActivity(), BaseActivity.AlertType.SUCCESS, "Profile Updated!", "GitHub URL Updated successfully");
            userData.setGit_url(url);
        } else {
//            displayAlert(requireActivity(), BaseActivity.AlertType.SUCCESS, "Profile Updated!", "LinkedIn URL Updated successfully");
            userData.setLikedin_url(url);
        }
        displayAlert(requireActivity(), BaseActivity.AlertType.SUCCESS, "Profile Updated!", "Information saved successfully");
        appConfig.saveUserConfig(userData);
    }

    @Override
    public void onSelectedCategoriesUpdated(ArrayList<JobCategoryModel> updatedJobCategoriesList) {
        appConfig.setDataUpdatedStatus(true);
        progressDialog.dismiss();
        this.selectedCategoryList.clear();
        this.selectedCategoryList.addAll(updatedJobCategoriesList);
        displayAlert(requireActivity(), BaseActivity.AlertType.SUCCESS, "Categories Saved!", "Preferred categories updated successfully");
        userData.setCategories(updatedJobCategoriesList);
        appConfig.saveUserConfig(userData);
        refreshStudentData();
    }

    @Override
    public void onOperationFailed(String error) {
        progressDialog.dismiss();
        displayAlert(requireActivity(), BaseActivity.AlertType.ERROR, "Error", error);
    }

    @Override
    public void onConnectionLost(String message) {
        progressDialog.dismiss();
        Toasty.error(requireContext(), message).show();
    }
}