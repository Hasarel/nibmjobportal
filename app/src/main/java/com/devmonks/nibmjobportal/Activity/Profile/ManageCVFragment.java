package com.devmonks.nibmjobportal.Activity.Profile;

import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.devmonks.nibmjobportal.Model.CVDocumentModel;
import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Adapters.ManageCVAdapter;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.Constraints;
import com.devmonks.nibmjobportal.Util.FadeAnimator;
import com.devmonks.nibmjobportal.Util.ItemOffsetDecoration;
import com.devmonks.nibmjobportal.Util.SingleOnClickListener;
import com.devmonks.nibmjobportal.Util.UIUtil;
import com.devmonks.nibmjobportal.databinding.FragmentManageCVBinding;
import com.hishd.lightpopup.LightPopup;
import com.rajat.pdfviewer.PdfViewerActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import es.dmoral.toasty.Toasty;

public class ManageCVFragment extends BaseFragment implements APIOperation.OnAPIResultCallback, ManageCVAdapter.CVItemClickListener {

    private final String TAG = "ManageCVFragment";
    private FragmentManageCVBinding binding;
    private UserModel userData = null;
    private ArrayList<CVDocumentModel> cvList = new ArrayList<>();
    private Animation recyclerAnimation;
    private ManageCVAdapter manageCVAdapter;
    private final com.devmonks.nibmjobportal.Util.UIUtil UIUtil = new UIUtil();
    private Dialog progressDialog;
    private String fileName;

    ActivityResultLauncher<Intent> activityResultLauncher;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentManageCVBinding.inflate(inflater);
        setupResources();
        initView();
        return binding.getRoot();
    }

    private void initView() {
        binding.btnAddCV.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                displayCVChooserPopup();
            }
        });

        binding.containerRefresh.setOnRefreshListener(() -> {
            binding.containerRefresh.setRefreshing(false);
            refreshCVList();
        });
    }

    private void setupResources() {
        recyclerAnimation = AnimationUtils.loadAnimation(requireContext(), R.anim.fade_enter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(requireContext(), R.anim.layout_animation_fall_down);
        final ItemOffsetDecoration itemOffsetDecoration = new ItemOffsetDecoration(requireContext(), R.dimen._5sdp, R.dimen._8sdp, R.dimen._1sdp, R.dimen._1sdp);
        binding.recyclerCV.setLayoutManager(new LinearLayoutManager(requireContext()));
        binding.recyclerCV.addItemDecoration(itemOffsetDecoration);
        binding.recyclerCV.setLayoutAnimation(controller);

        //Preparing adapter
        manageCVAdapter = new ManageCVAdapter(cvList, requireContext(), this, false);
        binding.recyclerCV.setAdapter(manageCVAdapter);

        //Preparing Progress overlay dialog
        progressDialog = UIUtil.getProgress(requireActivity());

        //Setting onActivityResult action for CV add popup
        setupOnActivityResult();
        refreshCVList();
    }

    private void setupOnActivityResult() {
        activityResultLauncher = registerForActivityResult(
                new ActivityResultContracts.StartActivityForResult(),
                result -> {
                    if(result.getData() == null) {
                        Toasty.info(requireContext(), "File selection cancelled!").show();
                        return;
                    }

                    if(result.getData().getData() == null) {
                        Toasty.error(requireContext(), "Could not read file information!").show();
                        return;
                    }

                    final Cursor returnCursor = requireContext().getContentResolver().
                            query(result.getData().getData(), null, null, null, null);
                    int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                    int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                    returnCursor.moveToFirst();

                    if(returnCursor.getLong(sizeIndex) / 1024 > Constraints.MAX_CV_SIZE)
                        Toasty.info(requireContext(), "File size too large. Maximum upload size is 512Kb").show();
                    else{
                        if (userData == null || userData.getNic() == null)
                            return;
//                        Log.e(TAG, returnCursor.getString(nameIndex).substring(returnCursor.getString(nameIndex).lastIndexOf(".")).toLowerCase());
                        if(!returnCursor.getString(nameIndex).substring(returnCursor.getString(nameIndex).lastIndexOf(".")).toLowerCase().equals(".pdf")) {
                            Toasty.error(requireContext(), "Please choose a valid PDF file!").show();
                            return;
                        }
                        progressDialog.show();
//                        apiOperation.uploadCVDocument(
//                                userData.getNic(),
//                                returnCursor.getString(nameIndex),
//                                this.fileName,
//                                new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).format(new Date()),
//                                result.getData().getData(),
//                                this
//                        );
                        new CVDocumentModel().uploadCVDocument(
                                userData.getNic(),
                                userData.getId(),
                                returnCursor.getString(nameIndex),
                                this.fileName,
                                new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).format(new Date()),
                                result.getData().getData(),
                                this
                        );
                    }
                    returnCursor.close();
                });
    }

    private void refreshCVList() {
        userData = appConfig.getUserConfig();
        if (userData == null || userData.getNic() == null)
            return;
        progressDialog.show();
//        apiOperation.getCVList(userData.getNic(), this);
        new CVDocumentModel().getCVList(userData.getNic(), userData.getId(), this);
    }

    private void displayCVChooserPopup() {
        if(cvList.size() >= Constraints.MAX_CV_LIMIT) {
            displayAlert(requireActivity(), BaseActivity.AlertType.WARNING, "Maximum Uploads", "Maximum CV upload count is 5. Please remove existing CVs and retry!");
            return;
        }

        new LightPopup(requireContext())
                .createCVUploadDialog()
                .setCancelledOnOutside(false)
                .setCVDialogBtn1Action((fileName, dialog) -> {
                    dialog.dismiss();
                    if(fileNameExists(fileName)) {
                        Toasty.error(requireContext(), "A CV with the provided name already exists!").show();
                        return;
                    }
                    this.fileName = fileName;
                    final Intent fileIntent = new Intent();
                    fileIntent.setAction(Intent.ACTION_GET_CONTENT);
                    fileIntent.setType("application/pdf");
                    activityResultLauncher.launch(fileIntent);
                }).show();
    }

    private boolean fileNameExists(String fileName) {
        for(CVDocumentModel cv: cvList) {
            if(fileName.equals(cv.getAlias()))
                return true;
        }

        return false;
    }

    private void setAnimationStatus(boolean status) {
        if (status) {
            FadeAnimator.fadeOutAnimation(binding.recyclerCV);
            binding.animationView.setVisibility(View.VISIBLE);
            binding.animationView.playAnimation();
        } else {
            binding.animationView.setVisibility(View.INVISIBLE);
            binding.animationView.cancelAnimation();
            FadeAnimator.fadeInAnimation(binding.recyclerCV);
        }
    }

    @Override
    public void onCVListLoaded(ArrayList<CVDocumentModel> cvList) {
        progressDialog.dismiss();
        if (cvList.size() == 0) {
            setAnimationStatus(true);
            return;
        } else {
            setAnimationStatus(false);
        }
        this.cvList.clear();
        this.cvList.addAll(cvList);
        this.manageCVAdapter.notifyDataSetChanged();
        binding.recyclerCV.startAnimation(recyclerAnimation);
    }

    @Override
    public void onCVRemoved() {
        progressDialog.dismiss();
        displayAlert(requireActivity(), BaseActivity.AlertType.SUCCESS, "Document Removed", "CV Document removed successfully!");
        refreshCVList();
    }

    @Override
    public void onCVAdded() {
        progressDialog.dismiss();
        displayAlert(requireActivity(), BaseActivity.AlertType.SUCCESS, "Document Added", "CV Document added successfully!");
        refreshCVList();
    }

    @Override
    public void onOperationFailed(String error) {
        progressDialog.dismiss();
        displayAlert(requireActivity(), BaseActivity.AlertType.ERROR, "Error", error);
    }

    @Override
    public void onConnectionLost(String message) {
        progressDialog.dismiss();
        Toasty.error(requireContext(), message).show();
    }

    @Override
    public void onCVItemClicked(int position) {
        startActivity(
                PdfViewerActivity.Companion.launchPdfFromUrl(requireContext(),
                        cvList.get(position).getDocument_url(),
                        cvList.get(position).getAlias(),
                        "",
                        false)
        );
    }

    @Override
    public void onDeleteClicked(int position) {
        new LightPopup(requireContext())
                .createDualActionDialog()
                .setCancelledOnOutside(false)
                .setTitle("Remove CV")
                .setMessage("Are you sure you want to remove the document '" + cvList.get(position).getAlias() + "'? This will permanently remove the document.")
                .setBtn1Caption("Remove")
                .setBtn1Color(R.color.red)
                .setBtn1Action(dialog -> {
                    if (userData == null || userData.getNic() == null)
                        return;
                    progressDialog.show();
//                    apiOperation.removeCV(userData.getNic(), cvList.get(position).get_id(), cvList.get(position).getDocument_url(), this);
                    new CVDocumentModel().removeCV(userData.getNic(), cvList.get(position).getId(), cvList.get(position).getDocument_url(), this);
                    dialog.dismiss();
                })
                .setBtn2Caption("Cancel")
                .setBtn2Color(R.color.blue_light)
                .setBtn2Action(Dialog::dismiss).show();
    }
}