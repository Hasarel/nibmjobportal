package com.devmonks.nibmjobportal.Activity.Auth;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.core.app.ActivityOptionsCompat;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.APIService.APIModel.StudentVerificationModel;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.Util.Validator;
import com.devmonks.nibmjobportal.databinding.ActivityValidateNicBinding;

import org.json.JSONException;

import spencerstudios.com.bungeelib.Bungee;

public class ValidateNICActivity extends BaseActivity implements APIOperation.OnAPIResultCallback {

    private ActivityValidateNicBinding binding;
    private Dialog studentVerificationInProgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityValidateNicBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        initView();
        setupResources();
    }

    private void setupResources() {
        studentVerificationInProgDialog = UIUtil.getProgress(ValidateNICActivity.this);
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        binding.getRoot().setOnTouchListener((v, event) -> {
            hideSoftKeyboard(ValidateNICActivity.this, v);
            return true;
        });

        binding.btnSignIn.setOnClickListener(v -> {
            onBackPressed();
        });

        binding.btnVerify.setOnClickListener(v -> {
            if (!Validator.isValidNIC(binding.txtNIC.getText().toString())) {
                binding.txtNIC.setError("Enter a valid NIC Number");
                vibrateDevice();
                return;
            }

            try {
                studentVerification(binding.txtNIC.getText().toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }

        });
    }

    private void studentVerification(String nic) throws JSONException {
        studentVerificationInProgDialog.show();
//        apiOperation.getStudentVerification(nic, this);
        new UserModel().getStudentVerification(nic, this);
    }

    /**
     * Integrated with newer API endpoint received from NIBM
     * @param model Object contains the student EXISTING status and NIC
     */
    @Override
    public void onStudentVerification(StudentVerificationModel model) {
        studentVerificationInProgDialog.dismiss();
        if(!model.isExist()) {
            displayAlert(this, AlertType.ERROR, "Verification Error!", "Student not found. Please contact NIBM!");
            return;
        }
        final ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, binding.imgLogo, "app_logo");
        startActivity(
                new Intent(this, RegisterActivity.class).putExtra("NIC", binding.txtNIC.getText().toString()),
                activityOptionsCompat.toBundle()
        );
        Bungee.fade(this);
    }

    @Override
    public void onOperationFailed(String error) {
        studentVerificationInProgDialog.dismiss();
        displayAlert(this, AlertType.ERROR, "Verification Error!", error);
    }

    @Override
    public void onConnectionLost(String message) {
        studentVerificationInProgDialog.dismiss();
        displayAlert(this, AlertType.WARNING, "Connection Error", message);
    }
}