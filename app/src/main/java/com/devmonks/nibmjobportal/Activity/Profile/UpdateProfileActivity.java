package com.devmonks.nibmjobportal.Activity.Profile;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.devmonks.nibmjobportal.APIService.APIModel.PasswordUpdateModel;
import com.devmonks.nibmjobportal.APIService.APIModel.StudentUpdateModel;
import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Adapters.ManageSkillsAdapter;
import com.devmonks.nibmjobportal.Model.SkillModel;
import com.devmonks.nibmjobportal.Model.UserModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.Constraints;
import com.devmonks.nibmjobportal.Util.ImageFetcher;
import com.devmonks.nibmjobportal.Util.ItemOffsetDecoration;
import com.devmonks.nibmjobportal.Util.SingleOnClickListener;
import com.devmonks.nibmjobportal.Util.Validator;
import com.devmonks.nibmjobportal.Util.YearList;
import com.devmonks.nibmjobportal.databinding.ActivityUpdateProfileBinding;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.hishd.lightpopup.LightPopup;
import com.hishd.multiselectpopup.MultiSelectDialog;
import com.hishd.multiselectpopup.MultiSelectModel;
import com.squareup.picasso.Picasso;

import org.aviran.cookiebar2.CookieBarDismissListener;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class UpdateProfileActivity extends BaseActivity implements APIOperation.OnAPIResultCallback, ImageFetcher.OnSelectedImageReadyListener {

    private ActivityUpdateProfileBinding binding;
    private UserModel userData = null;
    private ArrayList<SkillModel> selectedSkillList = new ArrayList<>();
    private ArrayList<SkillModel> availableSkillList = new ArrayList<>();
    private ArrayList<String> yearList;
    private List<String> qualificationList;
    private Animation recyclerAnimation;
    private ManageSkillsAdapter manageSkillsAdapter;
    private ArrayAdapter<String> yearArrayAdapter;
    private Dialog progressDialog;
    private Dialog uploadProgress;
    MultiSelectDialog selectSkillDialog;
    private final String TAG = "UpdateProfileActivity";
    private LightPopup lightPopupPassword;
    private ImageFetcher imageFetcher;
    ByteArrayOutputStream stream;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityUpdateProfileBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setupResources();
        initView();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initView() {
        binding.getRoot().setOnTouchListener((v, event) -> {
            hideSoftKeyboard(this, v);
            binding.txtName.clearFocus();
            binding.txtEmail.clearFocus();
            return true;
        });

        binding.imgProfile.setOnClickListener(v -> {
            imageFetcher.selectImage();
        });

        binding.btnManageSkills.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                getAvailableSkills();
            }
        });

        binding.btnChangePassword.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                lightPopupPassword.show();
            }
        });

        binding.btnUpdate.setOnClickListener(v -> {
            binding.txtAboutStudent.setError(null);
            binding.txtName.setError(null);
            binding.txtEmail.setError(null);

            if (binding.txtAboutStudent.getText().toString().length() > Constraints.MAX_STUDENT_ABOUT_LENGTH) {
                binding.txtAboutStudent.setError("Maximum characters should be " + Constraints.MAX_STUDENT_ABOUT_LENGTH);
                vibrateDevice();
                return;
            }

            if (!Validator.isValidName(binding.txtName.getText().toString())) {
                binding.txtName.setError("Enter a valid name between 5-50 characters.");
                vibrateDevice();
                return;
            }

            if (!Validator.isValidEmail(binding.txtEmail.getText().toString())) {
                binding.txtEmail.setError("Enter a valid email address.");
                vibrateDevice();
                return;
            }

            if (binding.spinnerQualification.getSelectedItemId() == 0) {
                displayAlert(this, AlertType.INFO, "Select Qualification", "The highest educational qualification should be selected.");
                return;
            }

            try {
                if (userData == null)
                    return;
                if (userData.getNic() != null) {
                    progressDialog.show();
//                    apiOperation.updateStudentInfo(
//                            userData.getNic(),
//                            binding.txtAboutStudent.getText().toString(),
//                            binding.txtName.getText().toString(),
//                            binding.txtEmail.getText().toString(),
//                            binding.spinnerQualification.getSelectedItemId() == 0 ? null : binding.spinnerQualification.getSelectedItem().toString(),
//                            binding.spinnerQualification.getSelectedItemId() == 0 ? null : binding.spinnerYear.getSelectedItem().toString(),
//                            this);
                    new UserModel().updateStudentInfo(
                            userData.getNic(),
                            userData.getId(),
                            binding.txtAboutStudent.getText().toString(),
                            binding.txtName.getText().toString(),
                            binding.txtEmail.getText().toString(),
                            binding.spinnerQualification.getSelectedItemId() == 0 ? null : binding.spinnerQualification.getSelectedItem().toString(),
                            binding.spinnerQualification.getSelectedItemId() == 0 ? null : binding.spinnerYear.getSelectedItem().toString(),
                            this);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                FirebaseCrashlytics.getInstance().recordException(e);
            }

        });

        binding.txtAboutStudent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (binding.txtAboutStudent.getText().length() > Constraints.MAX_STUDENT_ABOUT_LENGTH) {
                    binding.txtCharacterCount.setTextColor(ContextCompat.getColor(UpdateProfileActivity.this, R.color.red));
                } else {
                    binding.txtCharacterCount.setTextColor(ContextCompat.getColor(UpdateProfileActivity.this, R.color.dark_semi));
                }
                binding.txtCharacterCount.setText(String.valueOf(binding.txtAboutStudent.getText().length()));
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        imageFetcher.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imageFetcher.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void setupResources() {
        recyclerAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_enter);
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(this, R.anim.layout_animation_fall_down);
        final ItemOffsetDecoration itemOffsetDecoration = new ItemOffsetDecoration(this, R.dimen._1sdp, R.dimen._1sdp, R.dimen._5sdp, R.dimen._5sdp);
        binding.recyclerMySkills.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerMySkills.addItemDecoration(itemOffsetDecoration);
        binding.recyclerMySkills.setLayoutAnimation(controller);

        //Preparing Qualification Year Spinner and Qualification Name Spinner
        final int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        yearList = YearList.getYearsBetween(currentYear, currentYear - 10);
        qualificationList = Arrays.asList(this.getResources().getStringArray(R.array.qualification_level));
        yearArrayAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, yearList);
        yearArrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        binding.spinnerYear.setAdapter(yearArrayAdapter);

        //Preparing adapters
        manageSkillsAdapter = new ManageSkillsAdapter(this.selectedSkillList, this);
        //Setting adapters to RecyclerViews
        binding.recyclerMySkills.setAdapter(manageSkillsAdapter);

        //Preparing Progress overlay dialog
        progressDialog = UIUtil.getProgress(this);
        uploadProgress = UIUtil.getProgress(this, "Uploading Image...!");

        stream = new ByteArrayOutputStream();
        imageFetcher = new ImageFetcher(this, 60, this);

        binding.scrollView.post(() -> {
            binding.scrollView.smoothScrollTo(0, binding.scrollView.getHeight());
            new Handler().postDelayed(() -> binding.scrollView.smoothScrollTo(0, 0), 1000);
        });

        lightPopupPassword = new LightPopup(this)
                .createChangePasswordDialog()
                .setCancelledOnOutside(true)
                .setTitle("Change Password")
                .setBtn1Caption("Change Password")
                .setBtn1Color(R.color.blue_light)
                .setPwdDialogBtn1Action((password, dialog) -> {
                            dialog.dismiss();
                            try {
                                if (userData != null && userData.getNic() != null) {
                                    progressDialog.show();
//                                    apiOperation.updatePassword(userData.getNic(), password, UpdateProfileActivity.this);
                                    new UserModel().updatePassword(userData.getNic(), userData.getId(), password, UpdateProfileActivity.this);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                FirebaseCrashlytics.getInstance().recordException(e);
                            }
                        }, "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{6,20}$",
                        "Password should contain both uppercase, lowercase & special characters between 6-20 digits.");
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshStudentData(false);
    }

    private void getAvailableSkills() {
        progressDialog.show();
//        apiOperation.getAllSkillsList(this);
        new SkillModel().getAllSkillsList(this);
    }

    private void prepareManageSkillsSpinner(ArrayList<SkillModel> skillList) {
        userData = appConfig.getUserConfig();
        final ArrayList<MultiSelectModel> listOfSkills = new ArrayList<>();
        final ArrayList<Integer> selectedSkillID = new ArrayList<>();
        if (selectedSkillList.size() == 0) {
            for (int i = 0; i < availableSkillList.size(); i++)
                listOfSkills.add(new MultiSelectModel(i, availableSkillList.get(i).getName()));
        } else {
            for (int i = 0; i < availableSkillList.size(); i++) {
                for (SkillModel selectedSkill : selectedSkillList) {
                    if (availableSkillList.get(i).getName().equals(selectedSkill.getName()))
                        selectedSkillID.add(i);
                }
                listOfSkills.add(new MultiSelectModel(i, availableSkillList.get(i).getName()));
            }
        }

        selectSkillDialog = new MultiSelectDialog(this)
                .setTitle("Select Skills")
                .setTitleTypeface(ResourcesCompat.getFont(this, R.font.gilroy_bold))
                .setPositiveText("Done")
                .setNegativeText("Cancel")
                .setMinSelectionLimit(0)
                .setMaxSelectionLimit(availableSkillList.size())
                .preSelectIDsList(selectedSkillID)
                .multiSelectList(listOfSkills)
                .build()
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(ArrayList<Integer> selectedIds, ArrayList<String> selectedNames, String s) {
                        if (userData == null)
                            return;
                        if (userData.getNic() != null) {
                            updateMySkills(selectedIds, userData.getNic());
                        }
                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "Skill Selection cancelled");
                    }
                });

        try {
            selectSkillDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            FirebaseCrashlytics.getInstance().recordException(e);
        }
    }

    private void updateMySkills(ArrayList<Integer> selectedIds, String nic) {
        progressDialog.show();
        final ArrayList<SkillModel> skillList = new ArrayList<>();
        for (int id : selectedIds) {
            if (this.availableSkillList.size() > id)
                skillList.add(this.availableSkillList.get(id));
        }

//        apiOperation.updateSelectedSkills(skillList, nic, this);
        new SkillModel().updateSelectedSkills(skillList, nic, userData.getId(), this);
    }

    private void refreshStudentData(boolean refreshSkillsOnly) {
        userData = appConfig.getUserConfig();
        if (userData == null)
            return;

        if (!refreshSkillsOnly) {
            if (userData.getImage_url() != null)
                Picasso.get().load(userData.getImage_url()).placeholder(R.drawable.img_avatar).error(R.drawable.img_avatar).into(binding.imgProfile);
            if (userData.getAbout() != null) {
                binding.txtCharacterCount.setText(String.valueOf(userData.getAbout().length()));
                binding.txtAboutStudent.setText(userData.getAbout());
            } else {
                binding.txtAboutStudent.setHint("Add an introduction about yourself.");
                binding.txtCharacterCount.setText("0");
            }

            binding.txtName.setText(userData.getName());
            binding.txtEmail.setText(userData.getEmail());
            if (userData.getQulified_year() != null)
                binding.spinnerYear.setSelection(yearList.indexOf(userData.getQulified_year()));
            if (userData.getHigst_ql() != null)
                binding.spinnerQualification.setSelection(qualificationList.indexOf(userData.getHigst_ql()));
        }

        if (userData.getSkills() != null && userData.getSkills().size() != 0) {
            this.selectedSkillList.clear();
            this.selectedSkillList.addAll(userData.getSkills());
            this.manageSkillsAdapter.notifyDataSetChanged();
            binding.recyclerMySkills.startAnimation(recyclerAnimation);
            binding.txtAddSkillsMessage.setVisibility(View.INVISIBLE);
            binding.recyclerMySkills.setVisibility(View.VISIBLE);
        } else {
            binding.txtAddSkillsMessage.setVisibility(View.VISIBLE);
            binding.recyclerMySkills.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onImageReady(Bitmap bitmap, String error) {
        if (bitmap == null)
            return;

        binding.imgProfile.setImageBitmap(bitmap);
        if (userData == null)
            return;
        if (userData.getNic() != null) {
            uploadProgress.show();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 60, stream);
//            apiOperation.updateProfileImage(userData.getNic(), stream.toByteArray(), this);
            new UserModel().updateProfileImage(userData.getNic(), userData.getId(), stream.toByteArray(), this);
        }
    }

    @Override
    public void onAllSkillsLoaded(ArrayList<SkillModel> skillList) {
        progressDialog.dismiss();
        if (skillList.size() == 0) {
            Toasty.info(this, "No available skills found").show();
            return;
        }
        this.availableSkillList.clear();
        this.availableSkillList.addAll(skillList);
        prepareManageSkillsSpinner(skillList);
    }

    @Override
    public void onSelectedSkillsUpdated(ArrayList<SkillModel> skillList) {
        progressDialog.dismiss();
        this.selectedSkillList.clear();
        this.selectedSkillList.addAll(skillList);
        userData.setSkills(skillList);
        appConfig.saveUserConfig(userData);
        displayAlert(this, BaseActivity.AlertType.SUCCESS, "Skills Saved!", "Preferred skills updated successfully");
        refreshStudentData(true);
    }

    @Override
    public void onStudentInformationUpdated(StudentUpdateModel studentData) {
        progressDialog.dismiss();
//        userData.setAbout(studentData.getAbout());
        userData.setAbout(binding.txtAboutStudent.getText().toString());
//        userData.setName(studentData.getName());
        userData.setName(binding.txtName.getText().toString());
//        userData.setEmail(studentData.getEmail());
        userData.setEmail(binding.txtEmail.getText().toString());
//        userData.setHigst_ql(studentData.getHighest_qualification());
        userData.setHigst_ql(binding.spinnerQualification.getSelectedItem().toString());
//        userData.setQulified_year(studentData.getQualified_year());
        userData.setQulified_year(binding.spinnerYear.getSelectedItem().toString());
        appConfig.saveUserConfig(userData);
        binding.btnChangePassword.setEnabled(false);
        binding.btnManageSkills.setEnabled(false);
        binding.btnUpdate.setEnabled(false);
        displayAlert(this, AlertType.SUCCESS, "Data Saved!", "Student data updated successfully.", new CookieBarDismissListener() {
            @Override
            public void onDismiss(int dismissType) {
                finish();
            }
        });
//        refreshStudentData(false);
    }

    @Override
    public void onPasswordUpdated(PasswordUpdateModel result) {
        progressDialog.dismiss();
        displayAlert(this, AlertType.SUCCESS, "Password Changed!", "Passwords changed successfully.");
    }

    @Override
    public void onProfileImageUpdated(String url) {
        uploadProgress.dismiss();
        displayAlert(this, AlertType.SUCCESS, "Image Updated!", "Profile Image updated successfully.");
        userData.setImage_url(url);
        appConfig.saveUserConfig(userData);
        refreshStudentData(false);
    }

    @Override
    public void onOperationFailed(String error) {
        uploadProgress.dismiss();
        progressDialog.dismiss();
        displayAlert(this, BaseActivity.AlertType.ERROR, "Error", error);
    }

    @Override
    public void onConnectionLost(String message) {
        progressDialog.dismiss();
        Toasty.error(this, message).show();
    }
}