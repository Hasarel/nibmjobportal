package com.devmonks.nibmjobportal.Activity.JobVacancy;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.ContextCompat;

import com.devmonks.nibmjobportal.APIService.APIOperation;
import com.devmonks.nibmjobportal.Activity.BaseActivity;
import com.devmonks.nibmjobportal.Activity.Profile.UpdateProfileActivity;
import com.devmonks.nibmjobportal.Model.CVDocumentModel;
import com.devmonks.nibmjobportal.Model.JobVacancyModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.SingleOnClickListener;
import com.devmonks.nibmjobportal.databinding.ActivityApplyJobBinding;
import com.hishd.lightpopup.LightPopup;
import com.squareup.picasso.Picasso;

import java.util.Locale;

import es.dmoral.toasty.Toasty;
import spencerstudios.com.bungeelib.Bungee;

public class ApplyJobActivity extends BaseActivity implements APIOperation.OnAPIResultCallback {

    private ActivityApplyJobBinding binding;
    private JobVacancyModel jobVacancyModel;
    private CVDocumentModel selectedCV;
    private ActivityResultLauncher<Intent> cvResultLauncher;
    private Dialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityApplyJobBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setupResources();
        initView();
    }

    private void initView() {
        binding.btnBack.setOnClickListener(v -> {
            onBackPressed();
        });

        binding.btnApply.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                if (selectedCV == null) {
                    Toast.makeText(ApplyJobActivity.this, "Please select your CV", Toast.LENGTH_SHORT).show();
                    cvResultLauncher.launch(new Intent(ApplyJobActivity.this, SelectCVActivity.class));
                    Bungee.fade(ApplyJobActivity.this);
                    return;
                }
                verifyAndSubmit();
            }
        });

        binding.btnSelect.setOnClickListener(new SingleOnClickListener() {
            @Override
            public void performClick(View v) {
                cvResultLauncher.launch(new Intent(ApplyJobActivity.this, SelectCVActivity.class));
                Bungee.fade(ApplyJobActivity.this);
            }
        });
    }

    private void verifyAndSubmit() {
        new LightPopup(ApplyJobActivity.this)
                .createDualActionDialog()
                .setCancelledOnOutside(false)
                .setTitle("Apply Job")
                .setMessage("You are about to apply and submit your portfolio to this job position.")
                .setBtn1Caption("Cancel")
                .setBtn1Color(R.color.red)
                .setBtn1Action(Dialog::dismiss)
                .setBtn2Caption("Submit")
                .setBtn2Color(R.color.blue_light)
                .setBtn2Action(d -> {
                    d.dismiss();
                    applyForJob(selectedCV);
                }).show();
    }

    private void applyForJob(CVDocumentModel selectedCV) {
        if (this.jobVacancyModel != null && appConfig.getUserConfig().getNic() != null) {
            progressDialog.show();
//            apiOperation.applyForJob(appConfig.getUserConfig().getNic(), selectedCV.getDocument_url(), this.jobVacancyModel.getJob_id(), this);
            new JobVacancyModel().applyForJob(appConfig.getUserConfig().getNic(), appConfig.getUserConfig().getId(), selectedCV.getDocument_url(), this.jobVacancyModel.getId(), this);
        }
    }

    private void setupResources() {
        progressDialog = UIUtil.getProgress(this);
        if (getIntent().getSerializableExtra("JOB_DATA") != null) {
            this.jobVacancyModel = (JobVacancyModel) getIntent().getSerializableExtra("JOB_DATA");
            Picasso.get().load(jobVacancyModel.getCompanyLogo()).placeholder(R.drawable.img_loading).error(R.drawable.img_err).into(binding.imgCompanyLogo);
            binding.txtVacancyName.setText(jobVacancyModel.getPosition());
            binding.txtCompanyName.setText(jobVacancyModel.getCompanyName());
            binding.txtClosingDate.setText(String.format(Locale.ENGLISH, "Closing Date : %s", jobVacancyModel.getClosingDate().substring(0, 10).replace("-", ".")));
        }

        cvResultLauncher = registerForActivityResult(new ActivityResultContracts.StartActivityForResult(), result -> {
            if (result.getResultCode() == RESULT_OK) {
                if (result.getData() == null)
                    return;
                this.selectedCV = (CVDocumentModel) result.getData().getSerializableExtra("CV_DATA");
                refreshUI();
            }
        });

        refreshUI();
    }

    private void refreshUI() {
        if (this.selectedCV == null) {
            binding.containerCV.setVisibility(View.INVISIBLE);
//            binding.btnApply.setBackground(ContextCompat.getDrawable(this, R.drawable.button_dark_gray_round));
//            binding.btnApply.setEnabled(false);
            binding.btnApply.setText("Select CV to Apply");
        } else {
            binding.containerCV.setVisibility(View.VISIBLE);
            binding.btnApply.setBackground(ContextCompat.getDrawable(this, R.drawable.button_light_blue_round));
            binding.btnApply.setEnabled(true);
            binding.btnApply.setText("Apply");
            binding.txtCVName.setText(this.selectedCV.getAlias());
            binding.txtFileName.setText(String.format(Locale.ENGLISH, "File : %s", this.selectedCV.getFile_name()));
            binding.txtAddedDate.setText(String.format(Locale.ENGLISH, "Added date : %s", this.selectedCV.getAdded_date()));
        }
    }

    @Override
    public void onApplicationSuccess() {
        progressDialog.dismiss();
        binding.btnApply.setEnabled(false);
        binding.btnSelect.setEnabled(false);
        displayAlert(this, AlertType.SUCCESS, "Applied Successfully", "Your application was successfully submitted!", dismissType -> finish());
    }

    @Override
    public void onOperationFailed(String error) {
        progressDialog.dismiss();
        displayAlert(this, BaseActivity.AlertType.ERROR, "Error", error);
    }

    @Override
    public void onConnectionLost(String message) {
        progressDialog.dismiss();
        Toasty.error(this, message).show();
    }
}