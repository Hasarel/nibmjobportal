package com.devmonks.nibmjobportal.Activity.JobVacancy;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.databinding.ActivityViewFlyerBinding;
import com.squareup.picasso.Picasso;

public class ViewFlyerActivity extends AppCompatActivity {

    private ActivityViewFlyerBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityViewFlyerBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setupResources();
        initView();
    }

    private void initView() {
        binding.btnBack.setOnClickListener(v -> {
            finish();
        });
    }

    private void setupResources() {
        if(getIntent().hasExtra("URL")) {
            Picasso.get().load(getIntent().getStringExtra("URL")).into(binding.imgFlyer);
        }
    }
}