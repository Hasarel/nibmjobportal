package com.devmonks.nibmjobportal.Util;

public class Constraints {
    public static final String DB_NAME = "JOB_PORTAL";
    public static final String USER_CONFIG = "USER_CONFIG";
    public static final String USER_LOGGED = "USER_LOGGED";
    public static final String DATA_UPDATED = "DATA_UPDATED";
    public static final String CONNECTION_LOST = "An available internet connection is required to perform this operation.";

    public static final int MAX_STUDENT_ABOUT_LENGTH = 200;
    public static final long MAX_CV_SIZE = 512;
    public static final int MAX_CV_LIMIT = 5;
}
