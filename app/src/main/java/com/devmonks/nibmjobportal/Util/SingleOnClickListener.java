package com.devmonks.nibmjobportal.Util;

import android.os.SystemClock;
import android.view.View;

public abstract class SingleOnClickListener implements View.OnClickListener{

    protected int defaultInterval = 1000;
    private long lastTimeClicked = 0;

    public SingleOnClickListener() {
        this.defaultInterval = 1000;
    }

    public SingleOnClickListener(int interval) {
        this.defaultInterval = interval;
    }

    @Override
    public void onClick(View v) {
        if (SystemClock.elapsedRealtime() - lastTimeClicked < defaultInterval) {
            return;
        }
        lastTimeClicked = SystemClock.elapsedRealtime();
        performClick(v);
    }

    public abstract void performClick(View v);
}
