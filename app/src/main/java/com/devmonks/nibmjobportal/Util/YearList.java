package com.devmonks.nibmjobportal.Util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class YearList {
    public static ArrayList<String> getYearsBetween(int maxYear, int lowerYearBoundary) {
        final DateFormat formatter = new SimpleDateFormat("yyyy", Locale.ENGLISH);
        ArrayList<String> yearList = new ArrayList<>();
        Calendar beginCalender = Calendar.getInstance();
        Calendar endCalender = Calendar.getInstance();
        endCalender.set(Calendar.YEAR, maxYear + 1);
        beginCalender.set(Calendar.YEAR, lowerYearBoundary);

        while(beginCalender.before(endCalender)) {
            yearList.add(formatter.format(beginCalender.getTime()));
            beginCalender.add(Calendar.YEAR, 1);
        }

        return yearList;
    }
}
