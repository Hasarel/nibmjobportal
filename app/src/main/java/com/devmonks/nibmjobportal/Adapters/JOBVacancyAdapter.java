package com.devmonks.nibmjobportal.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devmonks.nibmjobportal.Model.JobVacancyModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.SingleOnClickListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class JOBVacancyAdapter extends RecyclerView.Adapter<JOBVacancyAdapter.ViewHolder> {

    ArrayList<JobVacancyModel> jobVacancyModelList;
    private final Context context;
    private JobVacancyModel jobVacancyModel;
    private OnJobVacancyClickedListener listener;

    public JOBVacancyAdapter(ArrayList<JobVacancyModel> jobVacancyModelList, Context context, OnJobVacancyClickedListener listener) {
        this.jobVacancyModelList = jobVacancyModelList;
        this.context = context;
        this.listener = listener;
    }

    public void setJobVacancyList(ArrayList<JobVacancyModel> jobVacancyModelList) {
        this.jobVacancyModelList = jobVacancyModelList;
        notifyDataSetChanged();
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_job_post, parent, false);
        return new JOBVacancyAdapter.ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        jobVacancyModel = jobVacancyModelList.get(position);
        holder.txtCompanyName.setText(jobVacancyModel.getCompanyName());
        holder.txtVacancyName.setText(jobVacancyModel.getPosition());
        holder.txtClosingDate.setText(String.format(Locale.ENGLISH, "Closing Date : %s", jobVacancyModel.getClosingDate().substring(0, 10).replace("-", ".")));
        Picasso.get().load(jobVacancyModel.getCompanyLogo()).placeholder(R.drawable.logo_placeholder).error(R.drawable.img_err).into(holder.imgCompanyLogo);
    }

    @Override
    public int getItemCount() {
        return jobVacancyModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        OnJobVacancyClickedListener listener;
        ImageView imgCompanyLogo;
        TextView txtVacancyName;
        TextView txtCompanyName;
        TextView txtClosingDate;

        public ViewHolder(@NonNull View itemView, OnJobVacancyClickedListener listener) {
            super(itemView);
            this.listener = listener;
            this.imgCompanyLogo = itemView.findViewById(R.id.imgCompanyLogo);
            this.txtVacancyName = itemView.findViewById(R.id.txtVacancyName);
            this.txtCompanyName = itemView.findViewById(R.id.txtCompanyName);
            this.txtClosingDate = itemView.findViewById(R.id.txtClosingDate);

            itemView.setOnClickListener(new SingleOnClickListener() {
                @Override
                public void performClick(View v) {
                    listener.onJobVacancySelected(getAdapterPosition(), jobVacancyModelList.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface OnJobVacancyClickedListener {
        void onJobVacancySelected(int position, JobVacancyModel data);
    }
}
