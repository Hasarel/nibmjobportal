package com.devmonks.nibmjobportal.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devmonks.nibmjobportal.Model.JobCategoryModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.SingleOnClickListener;

import java.util.ArrayList;
import java.util.Locale;

public class SelectedJobCategoryAdapter extends RecyclerView.Adapter<SelectedJobCategoryAdapter.ViewHolder> {

    private ArrayList<JobCategoryModel> jobCategoryList;
    private final Context context;
    private OnJobCategoryClickedListener listener;
    JobCategoryModel jobCategoryModel;

    public SelectedJobCategoryAdapter(ArrayList<JobCategoryModel> jobCategoryList, Context context, OnJobCategoryClickedListener listener) {
        this.jobCategoryList = jobCategoryList;
        this.context = context;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false);
        return new SelectedJobCategoryAdapter.ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        if (position == 0) {
            int count = 0;
            for (int i = 0; i < jobCategoryList.size(); i++)
                count += jobCategoryList.get(i).getJob_count();
            holder.txtCategoryName.setText("All Jobs");
            holder.txtJobCount.setText(String.format(Locale.ENGLISH, "%d Jobs", count));
            return;
        }
        jobCategoryModel = jobCategoryList.get(position);
        holder.txtCategoryName.setText(jobCategoryModel.getName());
        if (jobCategoryModel.getJob_count() == 1) {
            holder.txtJobCount.setText("1 Job");
        } else {
            holder.txtJobCount.setText(jobCategoryModel.getJob_count() > 100 ? "100+ Jobs" : String.format(Locale.ENGLISH, "%d Jobs", jobCategoryModel.getJob_count()));
        }
    }

    @Override
    public int getItemCount() {
        return jobCategoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        OnJobCategoryClickedListener listener;
        TextView txtCategoryName;
        TextView txtJobCount;

        public ViewHolder(@NonNull View itemView, OnJobCategoryClickedListener listener) {
            super(itemView);
            this.txtCategoryName = itemView.findViewById(R.id.txtCategoryName);
            this.txtJobCount = itemView.findViewById(R.id.txtJobCount);
            this.listener = listener;

            this.itemView.setOnClickListener(new SingleOnClickListener() {
                @Override
                public void performClick(View v) {
                    listener.onJobCategorySelected(getAdapterPosition(), jobCategoryList.get(getAdapterPosition()));
                }
            });
        }
    }

    public interface OnJobCategoryClickedListener {
        void onJobCategorySelected(int position, JobCategoryModel data);
    }
}
