package com.devmonks.nibmjobportal.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devmonks.nibmjobportal.Model.JobCategoryModel;
import com.devmonks.nibmjobportal.R;

import java.util.ArrayList;

public class ManageJobCategoryAdapter extends RecyclerView.Adapter<ManageJobCategoryAdapter.ViewHolder> {
    private ArrayList<JobCategoryModel> jobCategoryList;
    private final Context context;
    JobCategoryModel jobCategoryModel;

    public ManageJobCategoryAdapter(ArrayList<JobCategoryModel> jobCategoryList, Context context) {
        this.jobCategoryList = jobCategoryList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_profile_category, parent, false);
        return new ManageJobCategoryAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        jobCategoryModel = jobCategoryList.get(position);
        holder.txtCategoryName.setText(jobCategoryModel.getName());
    }

    @Override
    public int getItemCount() {
        return jobCategoryList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtCategoryName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txtCategoryName = itemView.findViewById(R.id.txtCategoryName);
        }
    }
}
