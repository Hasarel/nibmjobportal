package com.devmonks.nibmjobportal.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.devmonks.nibmjobportal.Model.CVDocumentModel;
import com.devmonks.nibmjobportal.R;
import com.devmonks.nibmjobportal.Util.SingleOnClickListener;

import java.util.ArrayList;
import java.util.Locale;

public class ManageCVAdapter extends RecyclerView.Adapter<ManageCVAdapter.ViewHolder> {

    ArrayList<CVDocumentModel> cvList;
    private final Context context;
    private CVDocumentModel cvItem;
    private CVItemClickListener listener;
    private int imgIndex = 0;
    private final boolean isSelectUI;

    public ManageCVAdapter(ArrayList<CVDocumentModel> cvList, Context context, CVItemClickListener listener, boolean isSelectUI) {
        this.cvList = cvList;
        this.context = context;
        this.listener = listener;
        this.isSelectUI = isSelectUI;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_cv, parent, false);
        return new ManageCVAdapter.ViewHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        cvItem = cvList.get(position);
        if (imgIndex == 3)
            imgIndex = 0;
        switch (imgIndex){
            case 0:
                holder.imgCV.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cv_blue));
                break;
            case 1:
                holder.imgCV.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cv_green));
                break;
            case 2:
                holder.imgCV.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_cv_orange));
                break;
        }
        imgIndex++;

        holder.txtCVName.setText(cvItem.getAlias());
        holder.txtFileName.setText(String.format(Locale.ENGLISH, "File : %s", cvItem.getFile_name()));
        holder.txtAddedDate.setText(String.format(Locale.ENGLISH, "Added date : %s", cvItem.getAdded_date()));
    }

    @Override
    public int getItemCount() {
        return cvList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imgCV;
        TextView txtCVName;
        TextView txtFileName;
        TextView txtAddedDate;
        Button btnDelete;
        private CVItemClickListener listener;

        public ViewHolder(@NonNull View itemView, CVItemClickListener listener) {
            super(itemView);
            this.listener = listener;
            this.imgCV = itemView.findViewById(R.id.imgCV);
            this.txtCVName = itemView.findViewById(R.id.txtCVName);
            this.txtFileName = itemView.findViewById(R.id.txtFileName);
            this.txtAddedDate = itemView.findViewById(R.id.txtAddedDate);
            this.btnDelete = itemView.findViewById(R.id.btnDelete);

            if(isSelectUI)
                this.btnDelete.setVisibility(View.INVISIBLE);
            else
                this.btnDelete.setVisibility(View.VISIBLE);

            itemView.setOnClickListener(new SingleOnClickListener() {
                @Override
                public void performClick(View v) {
                    listener.onCVItemClicked(getAdapterPosition());
                }
            });

            this.btnDelete.setOnClickListener(new SingleOnClickListener() {
                @Override
                public void performClick(View v) {
                    listener.onDeleteClicked(getAdapterPosition());
                }
            });

        }
    }

    public interface CVItemClickListener {
        void onCVItemClicked(int position);
        default void onDeleteClicked(int position) {}
    }
}
