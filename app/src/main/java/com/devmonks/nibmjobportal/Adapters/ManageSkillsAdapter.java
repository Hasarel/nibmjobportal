package com.devmonks.nibmjobportal.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.devmonks.nibmjobportal.Model.SkillModel;
import com.devmonks.nibmjobportal.R;

import java.util.ArrayList;

public class ManageSkillsAdapter extends RecyclerView.Adapter<ManageSkillsAdapter.ViewHolder> {
    private ArrayList<SkillModel> skillList;
    private final Context context;
    SkillModel skillModel;

    public ManageSkillsAdapter(ArrayList<SkillModel> skillList, Context context) {
        this.skillList = skillList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_profile_skill, parent, false);
        return new ManageSkillsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        skillModel = skillList.get(position);
        holder.txtSkillName.setText(skillModel.getName());
    }

    @Override
    public int getItemCount() {
        return skillList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtSkillName;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txtSkillName = itemView.findViewById(R.id.txtSkillName);
        }
    }
}
